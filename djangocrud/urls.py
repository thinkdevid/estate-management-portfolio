"""djangocrud URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import include, path
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from book import views
from django.conf.urls.static import static
from book.views import IndexTemplateTagihanView
from book.views import (
    BookAddView,
    BookEditView,
    IndexTemplateWargaView,
    BookDeleteView,
    AnalyticsIndexView,
    IndexTemplateTagihanView,
    ticket_class_view,
    TagihanAddView,
    WargaAddView,
    WargaEditView,
    GenerateLaporanWarga,
    GenerateLaporanIncome,
    export_input_bulan,
    GenerateChartIncome,
    CekTagihanWargaView,
    PdfTagihan,
    index,
    DetailTagihanWargaView,
    GenerateTagihanWargaView,
    GenerateInvoiceTagihanView,
    TagihanEditView,
    TagihanCetakView,
    IndexTemplateMutasiView,
    IndexTemplateSettingView,
    SettingEditView,
    jumlah_tagihan_warga,
    IndexTemplateArsipMutasiView,
    GenerateBkmView,
    DetailBkm,
    BkmCetakView,
    TahunanWargaView,
    WargaPenyesuaianView,
    CekPenyesuaianView, 
    IndexLaporanPenerimaanView,
)

urlpatterns = [
    #path('warga/', include('book.urls')),
    #path('tagihan/', include('book.urls')),
    url(r'^export/csv/$', views.export_wargas_csv, name='export_wargas_csv'),
    url(r'^export/input_bulan/$', views.export_input_bulan, name='export_input_bulan'),
    url(r'^export/export_mutasi_csv/$', views.export_mutasi_csv, name='export_mutasi_csv'),

    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', index, name='index'),
    
    url(r'^warga/$',IndexTemplateWargaView.as_view(), name='list_warga'),
    url(r'^laporan/penerimaan/$',IndexLaporanPenerimaanView.as_view(), name='list_laporan'),
    url(r'^mutasi/$',IndexTemplateMutasiView.as_view(), name='list_mutasi'),
    url(r'^arsip_mutasi/$',IndexTemplateArsipMutasiView.as_view(), name='list_arsip_mutasi'),
    url(r'^setting/$',IndexTemplateSettingView.as_view(), name='list_setting'),

    url(r'^warga/add/$', WargaAddView.as_view(), name='add_warga'),
    url(r'^warga/edit/(?P<fid>[\w.@+-]+)/$', WargaEditView.as_view(), name='edit_warga'),
    url(r'^warga/detail/(?P<fid>[\w.@+-]+)/$', DetailTagihanWargaView.as_view(), name='detail_tagihan_warga'),
    url(r'^warga/tahunan/(?P<fid>[\w.@+-]+)/$', TahunanWargaView.as_view(), name='tahunan_warga'),
    url(r'^warga/penyesuaian/$', WargaPenyesuaianView.as_view(), name='penyesuaian_warga'),
    url(r'^warga/cekpenyesuaian/$', CekPenyesuaianView.as_view(), name='cek_penyesuaian'),
    
    url(r'^tagihan/$',IndexTemplateTagihanView.as_view(), name='list_tagihan'),
    url(r'^tagihan/add/$', TagihanAddView.as_view(), name='add_tagihan'),
    url(r'^tagihan/edit/(?P<fid>[\w.@+-]+)/(?P<no_kavling>[\w.@+-]+)/$', TagihanEditView.as_view(), name='edit_tagihan'),
    url(r'^upload/csv/$', views.upload_csv, name='upload_csv'),
    
    url(r'^setting/edit/(?P<fid>[\w.@+-]+)/$', SettingEditView.as_view(), name='edit_setting'),
    
    #url(r'^bkm/$',GenerateBkmView.as_view(), name='generate_bkm'),
    url(r'^bkm/$', GenerateBkmView.as_view(), name='generate_bkm'),
    url(r'^bkm/detail/(?P<fid>[\w.@+-]+)/$', DetailBkm.as_view(), name='detail_bkm'),
    url(r'^bkm/cetak/(?P<fid>[\w.@+-]+)/$', BkmCetakView.as_view(), name='cetak_bkm'),

    url(r'^tagihan/cetak/(?P<fid>[\w.@+-]+)/(?P<no_kavling>[\w.@+-]+)/$', TagihanCetakView.as_view(), name='cetak_tagihan'),
    
    url(r'^laporan/$', GenerateLaporanIncome.as_view(), name='laporan'),
    url(r'^chart/$', ticket_class_view, name='chart_income'),
    url(r'^chart_count_income/$', jumlah_tagihan_warga, name='chart_count_income'),

    url(r'^tagihan/generate/$', GenerateTagihanWargaView.as_view(), name='generate_tagihan'),
    url(r'^tagihan/email/$', GenerateInvoiceTagihanView.as_view(), name='email_tagihan'),
    url(r'^tagihan/cek/$', CekTagihanWargaView.as_view(), name='cek_tagihan'),
]
#urlpatterns = [
 #   path('book/', include('book.urls')),
    #url(r'^$', IndexTemplateView.as_view(), name='index'),
    
    #url(r'^$', IndexTemplateTagihanView.as_view()),
    #url(r'^book/', include('book.urls')),
    #url(r'^tagihan/', include('book.urls')),
    #url(r'^warga/', include('book.urls'))
    
    #url(r'^pdf/tagihan/$', PdfTagihan.as_view(), name='PdfTagihan'),
    #
    #
    #url(r'^pdf/$', Pdf.as_view(), name='addpdf'),
    #
    #url(r'^warga/add/$', WargaAddView.as_view(), name='wargatagihan'),
    #url(r'^tagihan/add/$', TagihanAddView.as_view(), name='addtagihan'),
    
    
    
#]



