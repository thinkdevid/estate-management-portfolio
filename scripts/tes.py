import os
from django.utils import timezone
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djangocrud.settings")
from djangocrud import settings
import datetime
from datetime import datetime
from twilio.rest import Client
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import csv
import requests
from django.contrib import messages
from django.db.models.functions import ExtractYear
from django.db.models.functions import ExtractMonth
from book.models import (
    Warga,
    Tagihan,
    income,
    InputTagihan,
    Datamutasi,
    ArsipInputTagihan,
    ArsipDatamutasi,
    Setting,
    Mutasi,
    Hbb,
    Dbb,
    PenyesuaianWarga
)

def run():
    
    
    
    cek_tagihan()
    cek_penyesuaian()
    generate_bkm()
    
    #generate_tagihan()
    
    

def cek_penyesuaian():
    print ("Program otomatis CEK Penyesuaian")
    
    Data_penyesuaian = PenyesuaianWarga.objects.filter(status="Disetujui").values_list('no_va', 'penyesuaian_tunggakan','penyesuaian_denda','penyesuaian_sisa_bulan','status','id')        
    Tolak_Data_penyesuaian = PenyesuaianWarga.objects.filter(status="Pengajuan").values_list('id')
    i=0
    j=0

    for data in Tolak_Data_penyesuaian:
        tolak_Penyesuaian = PenyesuaianWarga.objects.get(id=data[0])
        tolak_Penyesuaian.status = "Batal"
        tolak_Penyesuaian.save()
        j=j+1

    for data in Data_penyesuaian:
        
        no_va = data[0]
        penyesuaian_tunggakan= data[1]
        penyesuaian_denda= data[2]
        penyesuaian_sisa_bulan= data[3]

        save_Penyesuaian = PenyesuaianWarga.objects.get(id=data[5])
        save_Penyesuaian.status = "Terupdate"
        save_Penyesuaian.save()

        save_Warga = Warga.objects.get(no_va=no_va)
        save_Warga.tunggakan = penyesuaian_tunggakan
        save_Warga.denda = penyesuaian_denda
        save_Warga.sisa_bulan = penyesuaian_sisa_bulan
        save_Warga.save()
        i=i+1
    print("data terupdate sebanyak =",i)
    print("data batal sebanyak =",j)
    
    print ("Cek Penyesuaian Berhasil")



def generate_bkm():
    print ("Program otomatis GENERATE BKM")
    mutasi = ArsipDatamutasi.objects.filter(branch_code = "").values_list('nominal', 'tglreport','nama_pelanggan','no_pelanggan','id')        
    temp_nominal = 0
    awal =1
    nomor=1
    nomor_dbb=0
    bulan = 1
    temp_bulan = bulan
    tahun = 2018
    temp_tgl = timezone.now()
    for i in mutasi: 
        
        print(i[4])
        datatagihan =i[3].strip()
        if datatagihan[0][0] == "9":    
            temp = "0"+datatagihan[1:11] # awalan no VA 9 diganti 0 
            datatagihan = temp
            kavling = Warga.objects.filter(no_va=datatagihan).values_list('no_kavling', flat=True)   
            print ("data mutasi =", datatagihan)                            
        else:
            kavling = Warga.objects.filter(no_va=i[3]).values_list('no_kavling', flat=True)   
        if awal == 1 :
            bulan = Bulan_romawi(i[1].month)
            tahun = i[1].year  
            temp_nominal = i[0]
            temp_tgl = i[1]
            awal = 0
            nomor_dbb = nomor_dbb +1
            save_dbb = Dbb(
                hbb_code ="BM/BCA/APS-"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                dbb_code ="BM/BCA/APS-"+str(tahun)+"/"+str(bulan)+"/"+str(nomor)+"/"+str(nomor_dbb),
                nominal =i[0],
                tglreport = i[1],
                nama_pelanggan = i[2], 
                no_kavling = kavling[0]
            )
            save_dbb.save()        

            flag_hbb = ArsipDatamutasi.objects.get(id=i[4])
            flag_hbb.branch_code = "1"
            flag_hbb.save()        

        else:
            if  temp_tgl == i[1]: #jika tgl report sama
                bulan = Bulan_romawi(i[1].month)
                tahun = i[1].year  
                
                temp_nominal = temp_nominal + i[0]
                nomor_dbb = nomor_dbb +1
                save_dbb = Dbb(
                    hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                    dbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor)+"/"+str(nomor_dbb),
                    nominal =i[0],
                    tglreport = i[1],
                    nama_pelanggan = i[2], 
                    no_kavling = kavling[0]
                )
                save_dbb.save()         
            else :
                save_hbb = Hbb(
                    hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                    nominal =temp_nominal,
                    tglreport = temp_tgl
                )
                bulan = Bulan_romawi(i[1].month)
                tahun = i[1].year  
                save_hbb.save()                                         
                print(nomor)
                nomor=nomor+1                    
                if temp_bulan != bulan : #reset no ganti bulan
                    nomor = 1
                    temp_bulan = bulan
                nomor_dbb=1
                save_dbb = Dbb(
                    hbb_code ="BM/BCA/APS-"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                    dbb_code ="BM/BCA/APS-"+str(tahun)+"/"+str(bulan)+"/"+str(nomor)+"/"+str(nomor_dbb),
                    nominal =i[0],
                    tglreport = i[1],
                    nama_pelanggan = i[2], 
                    no_kavling = kavling[0]
                )
                save_dbb.save()                
                temp_nominal = i[0]
                temp_tgl = i[1]

            flag_hbb = ArsipDatamutasi.objects.get(id=i[4])
            flag_hbb.branch_code = "1"
            flag_hbb.save()        

    #input terakhir
    save_hbb = Hbb(
        hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
        nominal =temp_nominal,
        tglreport = temp_tgl
    )
    save_hbb.save() 
    print ("Program otomatis Generate BKM Berhasil")
    print(str(nomor))

def cek_tagihan():
    print ("Program otomatis CEK Tagihan")

    num_tagihan=InputTagihan.objects.all().count()
    if num_tagihan > 0 :

        i = 0
        j = 0
        k = 0
        cek_ada=0
        listtagihan = InputTagihan.objects.all().values_list('no_va','denda','nilai_pokok','tunggakan','id') #datatagihan
        listmutasi = Datamutasi.objects.filter(company_code='07458').values_list('no_pelanggan','tanggal','nominal') #datamutasi masuk
        listtagihanall = Tagihan.objects.all().values_list('id','no_kavling') #datamutasi masuk
        jatuh_tempo = datetime(datetime.now().year, datetime.now().month, 15)
        persen_denda = Setting.objects.get().denda

        for tagihan in listtagihan: # sebanyak isi table inputtagihan
            print (i , tagihan[0])
            datatagihan =tagihan[0].strip()
            id_tagihan = tagihan[4]
            if datatagihan[0][0] != "9":  # jika tidak bayar tahunan
                iuran_ipl = Warga.objects.get(no_va=datatagihan).ipl
                tunggakan_denda =  Warga.objects.get(no_va=datatagihan).tunggakan_denda
            for mutasi in listmutasi: # sebanyak data di table _datamutasi
                datamutasi =mutasi[0].strip()
                if datatagihan == datamutasi: #(jika dibayar)
                    if datatagihan[0][0] == "9":    
                        temp = "0"+datatagihan[1:11] # awalan no VA 9 diganti 0 
                        datatagihan = temp
                        print ("data mutasi =", datatagihan)
                    cek_sisa_bulan = Warga.objects.filter(no_va=datatagihan).values_list('sisa_bulan', flat=True)   # cek pembayaran tahunan
                    print (cek_sisa_bulan[0])
                    cek_sisa_bulan = int(cek_sisa_bulan[0])
                    print (cek_sisa_bulan)
                    if cek_sisa_bulan < 0 :
                        print ("masuk deposit tahunan")
                        #cek_sisa_bulan = cek_sisa_bulan + (2*cek_sisa_bulan)
                        cek_sisa_bulan = -1 * cek_sisa_bulan
                        print ("masuk deposit tahunan")
                        Warga.objects.filter(no_kavling=datatagihan).update(sisa_bulan=cek_sisa_bulan)
                        #Tagihan.objects.filter(no_kavling=datatagihan).update(sisa_bulan=cek_sisa_bulan)

                    k=k+1
                    print ('bayar:'+datatagihan)
                    print (id_tagihan)
                    Tagihan.objects.filter(no_kavling=datatagihan).update(status="Lunas") #update status tagihan = Lunas                 
                    #CetakKwitansiView.get(self, request,id_tagihan,datatagihan) #sent email kwitansi
                
                    Warga.objects.filter(no_va=datatagihan).update(tunggakan=0) #update tunggakan tagihan = 0
                    Warga.objects.filter(no_va=datatagihan).update(bayar_lalu=mutasi[2]) #update pembayaran bulan lalu

                    if mutasi[1] > datetime.date(jatuh_tempo): 
                        bayar_denda = persen_denda * iuran_ipl /100
                        #Warga.objects.filter(no_va=datatagihan).update(denda=bayar_denda) #update denda tagihan = 0 disable denda
                        #hitung_tunggakan_denda = bayar_denda + tunggakan_denda                        
                        #Warga.objects.filter(no_va=datatagihan).update(tunggakan_denda=hitung_tunggakan_denda) #update denda tagihan = 0
                    else :
                        Warga.objects.filter(no_va=datatagihan).update(denda=0) #update denda tagihan = 0
                        Warga.objects.filter(no_va=datatagihan).update(tunggakan_denda=0) #update denda tagihan = 0
                    
                    cek_sisa_bulan = Warga.objects.filter(no_va=datatagihan).values_list('sisa_bulan', flat=True)   # cek pembayaran tahunan                    
                    cek_sisa_bulan = int(cek_sisa_bulan[0])
                    if cek_sisa_bulan<0 : # jika bayar tahunan
                        #cek_sisa_bulan = cek_sisa_bulan + (2*cek_sisa_bulan)
                        cek_sisa_bulan = -1 * cek_sisa_bulan
                        print ("masuk deposit tahunan2")
                        Warga.objects.filter(no_kavling=datatagihan).update(sisa_bulan=cek_sisa_bulan)
                        #Tagihan.objects.filter(no_kavling=datatagihan).update(sisa_bulan=cek_sisa_bulan)
                        #CetakKwitansiView.get(self, request,id_tagihan,datatagihan) #sent email kwitansi
                    i = i + 1
                    cek_ada = 1

        #(jika tidak bayar)
            if cek_ada == 0 :
                print ('tidak bayar:'+datatagihan)
                j= j + 1
                bayar_denda = persen_denda * iuran_ipl /100
                hitung_tunggakan =tagihan[2]+tagihan[3] #IPL+tunggakan tidak hitung admin_VA
                Warga.objects.filter(no_va=datatagihan).update(tunggakan=hitung_tunggakan) #update tunggakan master warga
            cek_ada = 0
            i=i+1
        

        backup_inputtagihan = InputTagihan.objects.all().values()
        for x in backup_inputtagihan:
            ArsipInputTagihan.objects.create(**x)
            InputTagihan.objects.all().delete()

        backup_datamutasi = Datamutasi.objects.all().values()
        for y in backup_datamutasi:
            ArsipDatamutasi.objects.create(**y)
            Datamutasi.objects.all().delete()

        print ("Total Tagihan tercek" ,k+j, "Pembayaran")
        print ("Pembayaran diterima Sebanyak" ,k, "Pembayaran")
        print ("Pembayaran belum diterima Sebanyak" ,j, "Pembayaran")
        print("Transaksi Berhasil")
    else:
        print("Transaksi Gagal,Tidak ada Payment yang diproses")

#########################################################################################################################

def generate_tagihan():
    print ("Program otomatis GENERATE Tagihan")

    hari_ini = datetime.today()
    bulan_ini = (hari_ini.month+1 ) % 12 #upload before 31 bulan
    #bulan_ini = (hari_ini.month ) % 12 #upload after 31
    bulan_tagihan = datetime(hari_ini.year, bulan_ini, 1)
    num_tagihan=InputTagihan.objects.all().count()
    if num_tagihan > 0 :
        print("Transaksi Gagal,Tagihan sudah pernah diproses, lakukan cek payment terlebih dahulu")
        
    else:
        wargas = Warga.objects.all().values_list('no_va', 'nama', 'ipl','tunggakan','denda','lain','sisa_bulan','saldo')
        for warga in wargas:
            if  warga[6] <= 0 :   # jika tidak punya saldo         
                saveinput_tagihan = InputTagihan(
                    no_va =warga[0],
                    nama_pelanggan =warga[1],
                    nilai_pokok = warga[2],
                    tunggakan =warga[3],
                    denda = warga[4],
                    admin = warga[5],
                )
                saveinput_tagihan.save()
                saveTagihan = Tagihan(
                    tanggal = bulan_tagihan,
                    meteran_air_lalu =0,
                    meteran_air_sekarang =  0,              
                    pemakaian_air = 0,                
                    biaya_admin = warga[5],
                    biaya_perawatan = 0,
                    no_kavling = warga[0],
                    iuran_estate = warga[2],
                    tunggakan = warga[3],
                    total_pembayaran_air = 0,
                    total_keseluruhan = warga[2]+warga[3]+warga[4]+warga[5],
                    jatuh_tempo = datetime(datetime.now().year, bulan_ini, 15),
                    denda = warga[4]
                )     
                saveTagihan.save()  
                #CetakInvoiceView.get(self, request,saveTagihan.id,saveTagihan.no_kavling) #sent email
            else : # jika punya saldo
                no_va= warga[0]
                sisa_bulan = warga[6]-1
                Warga.objects.filter(no_va=no_va).update(sisa_bulan=sisa_bulan)
                Warga.objects.filter(no_va=no_va).update(tunggakan=0)
                Warga.objects.filter(no_va=no_va).update(saldo=0)
        print("Transaksi Berhasil")
            

def Terbilang(x):
    satuan=["","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"]
    n = int(x)
    if n >= 0 and n <= 11:
        Hasil = " " + satuan[n]
    elif n >= 12 and n <= 19:
        Hasil = Terbilang(n % 10) + " Belas"
    elif n >= 20 and n <= 99:
        Hasil = Terbilang(n / 10) + " Puluh" + Terbilang(n % 10)
    elif n >= 100 and n <= 199:
        Hasil = " Seratus" + Terbilang(n - 100)
    elif n >= 200 and n <= 999:
        Hasil = Terbilang(n / 100) + " Ratus" + Terbilang(n % 100)
    elif n >= 1000 and n <= 1999:
        Hasil = " Seribu" + Terbilang(n - 1000)
    elif n >= 2000 and n <= 999999:
        Hasil = Terbilang(n / 1000) + " Ribu" + Terbilang(n % 1000)
    elif n >= 1000000 and n <= 999999999:
        Hasil = Terbilang(n / 1000000) + " Juta" + Terbilang(n % 1000000)
    else:
        Hasil = Terbilang(n / 1000000000) + " Milyar" + Terbilang(n % 100000000)

    return Hasil

def get_Tahun(x):

    n = ExtractYear(x)
    
    return n

def Bulan_romawi(x):

    n = x
    if n == 1 :
        Hasil = "I"
    elif n == 2:
        Hasil = "II"
    elif n == 3:
        Hasil = "III"
    elif n == 4:
        Hasil = "IV"
    elif n == 5:
        Hasil = "V"
    elif n == 6:
        Hasil = "VI"
    elif n == 7:
        Hasil = "VII"
    elif n == 8:
        Hasil = "VIII"
    elif n == 9:
        Hasil = "IX"
    elif n == 10:
        Hasil = "X"
    elif n == 11:
        Hasil = "XI"
    else:
        Hasil = "XII"

    return Hasil



def CetakInvoiceView(self, request,fid,no_va):
    context={
        #'tagihan' : Tagihan.objects.get(id=fid),          
        #'warga' : Warga.objects.filter(no_kavling='B91')
        #'warga' : Warga.objects.get(no_kavling='B91').nama #select meter_air from warga where no kavling = x
    }
    #print (no_kavling)
    list_tagihan = Tagihan.objects.filter(id=fid)
    status = Tagihan.objects.get(id=fid).status 
    list_warga = Warga.objects.filter(no_va=no_va)
    email = Warga.objects.get(no_va=no_va).email
    today = timezone.now()
    params = {
        
        'today': today,
        'list_tagihan': list_tagihan,
        'list_warga': list_warga,
        'request': request
    }
    if status == "Belum Terbayar" and email != "-" :
    #elif status == "Belum Terbayar" :
        print ("masuk tagihan")
        print (email)
        file = Render.render_to_file('cetaktagihan.html', params)
        thread = Thread(target=send_simple_message_invoice, args=(file,email,))
        thread.start()
        return Render.render('cetaktagihan.html', params)


def CetakKwitansiView(self, request,fid,no_kavling):
    context={
        
    }
    list_tagihan = Tagihan.objects.filter(id=fid)
    status = Tagihan.objects.get(id=fid).status 
    list_warga = Warga.objects.filter(no_va=no_kavling)
    email = Warga.objects.get(no_va=no_kavling).email
    today = timezone.now()
    params = {
        
        'today': today,
        'list_tagihan': list_tagihan,
        'list_warga': list_warga,
        'request': request
    }
    #print (status)
    #send_simple_message2()
    #send_sms("nico","27 May 1989")
    print (fid)
    print (email)
    print (status)
    #if status == "Lunas" and email != "-":
    if  email != "-":
        print ("masuk sent email")
        print (email)
        file = Render.render_to_file('cetakkwitansi.html', params)
        thread = Thread(target=send_simple_message_kwitansi, args=(file,email,))
        thread.start()
        return Render.render('cetakkwitansi.html', params)
