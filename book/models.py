from django.db import models
from django.urls import reverse
from django.db.models import Func

class Month(Func):
    function = 'EXTRACT'
    template = '%(function)s(MONTH from %(expressions)s)'
    output_field = models.IntegerField()

# Create your models here.

class Mutasi(models.Model):
    no_pelanggan = models.CharField(max_length=765, blank=True, null=True)
    tanggal = models.DateField(blank=True, null=True)
    nama_pelanggan = models.CharField(max_length=48, blank=True, null=True)
    no_kavling = models.CharField(max_length=150, blank=True, null=True)
    nominal = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mutasi'
        
class Datamutasi(models.Model):
    company_code = models.CharField(max_length=20, blank=True, null=True)
    branch_code = models.CharField(max_length=20, blank=True, null=True)
    subcompany_code = models.CharField(max_length=20, blank=True, null=True)
    no_urut = models.IntegerField(blank=True, null=True)
    no_pelanggan = models.CharField(max_length=255, blank=True, null=True)
    mata_uang = models.CharField(max_length=3, blank=True, null=True)
    nominal = models.FloatField(blank=True, null=True)
    tanggal = models.DateField(blank=True, null=True)
    jam = models.TimeField(blank=True, null=True)
    lokasi = models.CharField(max_length=20, blank=True, null=True)
    berita1 = models.TextField(blank=True, null=True)
    berita2 = models.TextField(blank=True, null=True)
    uploadtime = models.DateField(blank=True, null=True)
    authors = models.TextField(blank=True, null=True)
    tglreport = models.DateField(blank=True, null=True)
    nama_pelanggan = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        managed = True
        db_table = '_datamutasi'

class ArsipDatamutasi(models.Model):
    company_code = models.CharField(max_length=20, blank=True, null=True)
    branch_code = models.CharField(max_length=20, blank=True, null=True)
    subcompany_code = models.CharField(max_length=20, blank=True, null=True)
    no_urut = models.IntegerField(blank=True, null=True)
    no_pelanggan = models.CharField(max_length=255, blank=True, null=True)
    mata_uang = models.CharField(max_length=3, blank=True, null=True)
    nominal = models.IntegerField(blank=True, null=True)
    tanggal = models.DateField(blank=True, null=True)
    jam = models.TimeField(blank=True, null=True)
    lokasi = models.CharField(max_length=20, blank=True, null=True)
    berita1 = models.TextField(blank=True, null=True)
    berita2 = models.TextField(blank=True, null=True)
    uploadtime = models.DateField(blank=True, null=True)
    authors = models.TextField(blank=True, null=True)
    tglreport = models.DateField(blank=True, null=True)
    nama_pelanggan = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        db_table = 'ArsipDataMutasi'

class Hbb(models.Model):
    hbb_code = models.CharField(max_length=50, blank=True, null=True)
    nominal = models.IntegerField(blank=True, null=True)
    tglreport = models.DateField(blank=True, null=True)

    class Meta:
        db_table = 'hbb'

class Dbb(models.Model):
    hbb_code = models.CharField(max_length=50, blank=True, null=True)
    dbb_code = models.CharField(max_length=50, blank=True, null=True)
    nominal = models.IntegerField(blank=True, null=True)
    tglreport = models.DateField(blank=True, null=True)
    nama_pelanggan = models.CharField(max_length=16, blank=True, null=True)
    no_kavling = models.CharField(max_length=150,blank=True, null=True, help_text='Pilih no kavling')
    
    class Meta:
        db_table = 'dbb'

class Setting(models.Model):
    company_code = models.CharField(max_length=20, blank=True, null=True)
    singkatan_pt = models.CharField(max_length=20, blank=True, null=True)
    nama_proyek = models.CharField(max_length=50, blank=True, null=True)
    nama_pt = models.CharField(max_length=50, blank=True, null=True)
    website = models.CharField(max_length=50, blank=True, null=True)
    admin = models.IntegerField(blank=True, null=True)
    IPL = models.IntegerField(blank=True, null=True)
    denda = models.IntegerField(blank=True, null=True)
    

    class Meta:
        db_table = 'setting'

class Passenger(models.Model):
    name = models.CharField(max_length=150,)
    sex = models.CharField(max_length=150,)
    survived = models.BooleanField(max_length=150,)
    age = models.FloatField()
    ticket_class = models.PositiveSmallIntegerField()
    embarked = models.CharField(max_length=150,)

class income(models.Model):
    tahun_bulan = models.IntegerField()
    total_tagihan = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'total_tagihan' # your view name

class Book(models.Model):

    title = models.CharField(max_length=150,blank=True, null=True)
    author = models.CharField(max_length=150,blank=True, null=True)
    data_published = models.DateField(blank=True, null=True)
    number_of_page = models.IntegerField(blank=True, null=True)
    type_of_book = models.CharField(max_length=35,blank=True, null=True)

    class Meta:
        db_table = 'book'


class InputTagihan(models.Model):

    no_va = models.CharField(max_length=150,blank=True, null=True)
    nama_pelanggan = models.CharField(max_length=150,blank=True, null=True)
    nilai_pokok = models.IntegerField(blank=True, null=True)
    tunggakan = models.IntegerField(blank=True, null=True)
    denda = models.IntegerField(blank=True, null=True)
    admin = models.IntegerField(blank=True, null=True)
    deposit= models.IntegerField(blank=True, null=True,default=0, help_text="deposit rupiah")
   
    class Meta:
        db_table = 'InputTagihan'

class ArsipInputTagihan(models.Model):

    no_va = models.CharField(max_length=150,blank=True, null=True)
    nama_pelanggan = models.CharField(max_length=150,blank=True, null=True)
    nilai_pokok = models.IntegerField(blank=True, null=True)
    tunggakan = models.IntegerField(blank=True, null=True)
    denda = models.IntegerField(blank=True, null=True)
    admin = models.IntegerField(blank=True, null=True)
    deposit= models.IntegerField(blank=True, null=True,default=0, help_text="deposit rupiah")
   
    class Meta:
        db_table = 'ArsipInputTagihan'

class Tagihan(models.Model):

    tanggal = models.DateField(max_length=150,blank=True, null=True)
    tanggal_bayar = models.DateField(max_length=150,blank=True, null=True)
    no_kavling = models.CharField(max_length=150,blank=True, null=True)
    proyek = models.CharField(max_length=150,blank=True, null=True)
    meteran_air_lalu = models.IntegerField(blank=True, null=True)
    meteran_air_sekarang = models.IntegerField(blank=True, null=True)
    pemakaian_air = models.IntegerField(blank=True, null=True)
    biaya_admin = models.IntegerField(blank=True, null=True)
    biaya_materai = models.IntegerField(blank=True, null=True)
    biaya_perawatan = models.IntegerField(blank=True, null=True)
    iuran_estate = models.IntegerField(blank=True, null=True)
    iuran_rt = models.IntegerField(blank=True, null=True)
    tunggakan = models.IntegerField(blank=True, null=True)
    denda = models.IntegerField(blank=True, null=True)
    total_pembayaran_air = models.IntegerField(blank=True, null=True)
    total_keseluruhan = models.IntegerField(blank=True, null=True)
    denda = models.IntegerField(blank=True, null=True)
    jatuh_tempo = models.DateField(max_length=150,blank=True, null=True)
    status = models.CharField(max_length=20,blank=False, null=False,default='Belum Terbayar')
    deposit= models.IntegerField(blank=True, null=True,default=0, help_text="deposit rupiah")
    
   
    class Meta:
        db_table = 'tagihan'

class Kavling(models.Model):
    """
    Model representing a book genre (e.g. Science Fiction, Non Fiction).
    """
    no_kavling = models.CharField(max_length=200, help_text="isi no kavling (cth. AA-01, B30 dll.)")
    nama_proyek = models.CharField(max_length=200,blank=True, null=True, help_text="isi no kavling (cth. AA-01, B30 dll.)")
   
    def __str__(self):
        """
        String for representing the Model object (in Admin site etc.)
        """
        return self.no_kavling
    
    class Meta:
        db_table = 'kavling'


class Proyek(models.Model):
    """
    Model representing a book genre (e.g. Science Fiction, Non Fiction).
    """
    nama_proyek = models.CharField(max_length=200, help_text="isi nama proyek ")
   
    def __str__(self):
        """
        String for representing the Model object (in Admin site etc.)
        """
        return self.nama_proyek
    class Meta:
        db_table = 'proyek'


class Warga(models.Model):

    no_va = models.CharField(max_length=150,blank=True, null=True)
    nama = models.CharField(max_length=150,blank=True, null=True)
    nama_proyek = models.CharField(max_length=150,blank=True, null=True, help_text='Pilih proyek')
    no_kavling = models.CharField(max_length=150,blank=True, null=True, help_text='Pilih no kavling')
    email = models.EmailField(max_length=150,blank=True, null=True)
    hp = models.CharField(max_length=150,blank=True, null=True)
    meter_air = models.IntegerField(blank=True, null=True, help_text="isi meteran air")
    luas_tanah = models.IntegerField(blank=True, null=True, help_text="isi Luas tanah")
    ipl = models.IntegerField(blank=True, null=True, help_text="isi iuran IPL")
    saldo = models.IntegerField(blank=True,default='0', null=True, help_text="isi Saldo")
    status = models.CharField(max_length=10,default='0',  help_text="isi sanksi , 0 = baik , 3 = buruk")
    tunggakan_denda = models.IntegerField(blank=True,default='0', null=True, help_text="isi iuran IPL")
    tunggakan = models.IntegerField(blank=True,default='0', null=True, help_text="isi tunggakan_denda")
    denda = models.IntegerField(blank=True,default='0', null=True, help_text="isi Saldo")
    lain = models.IntegerField(blank=True,default='0', null=True, help_text="isi Saldo")
    sisa_bulan = models.IntegerField(blank=True,default='0', null=True,help_text="sisa deposit bulan")
    bayar_lalu = models.IntegerField(blank=True,default='0', null=True, help_text="pembayaran bulan lalu")
    deposit= models.IntegerField(blank=True, null=True,default=0, help_text="deposit rupiah")

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.nama
    def get_absolute_url(self):
        """
        Returns the url to access a detail record for this warga.
        """
        return reverse('warga-detail', args=[str(self.id)])

    class Meta:
        db_table = 'warga'


class PenyesuaianWarga(models.Model):

    no_va = models.CharField(max_length=150,blank=True, null=True)
    '''
    Penyesuaian_nama = models.CharField(max_length=150,blank=True, null=True)
    Penyesuaian_nama_proyek = models.CharField(max_length=150,blank=True, null=True, help_text='Pilih proyek')
    Penyesuaian_no_kavling = models.CharField(max_length=150,blank=True, null=True, help_text='Pilih no kavling')
    Penyesuaian_email = models.EmailField(max_length=150,blank=True, null=True)
    Penyesuaian_hp = models.CharField(max_length=150,blank=True, null=True)
    Penyesuaian_meter_air = models.IntegerField(blank=True, null=True, help_text="isi meteran air")
    Penyesuaian_luas_tanah = models.IntegerField(blank=True, null=True, help_text="isi Luas tanah")
    Penyesuaian_ipl = models.IntegerField(blank=True, null=True, help_text="isi iuran IPL")
    Penyesuaian_saldo = models.IntegerField(blank=True, null=True, help_text="isi Saldo")
    Penyesuaian_status = models.CharField(max_length=10,default='0',  help_text="isi sanksi , 0 = baik , 3 = buruk")
    Penyesuaian_tunggakan_denda = models.IntegerField(blank=True, null=True, help_text="isi iuran IPL")    
    Penyesuaian_lain = models.IntegerField(blank=True, null=True, help_text="isi Saldo")
    Penyesuaian_bayar_lalu = models.IntegerField(blank=True, null=True,default=0, help_text="pembayaran bulan lalu")
    '''
    penyesuaian_tunggakan = models.IntegerField(blank=True,default='0', null=True, help_text="isi tunggakan_denda")
    penyesuaian_denda = models.IntegerField(blank=True, null=True, help_text="isi Saldo")
    penyesuaian_sisa_bulan = models.IntegerField(blank=True, null=True,default=0, help_text="sisa deposit bulan")
    deposit= models.IntegerField(blank=True, null=True,default=0, help_text="sisa deposit bulan")
    tanggal = models.DateField(max_length=150,blank=True, null=True)
    status = models.CharField(max_length=50,blank=True, null=True, help_text='Pilih proyek')
    user = models.CharField(max_length=50,blank=True, null=True, help_text='Pilih proyek')

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.nama
    def get_absolute_url(self):
        """
        Returns the url to access a detail record for this warga.
        """
        return reverse('warga-detail', args=[str(self.id)])

    class Meta:
        db_table = 'penyesuaianwarga'

class LaporanPenerimaan(models.Model):

    tanggal = models.DateField(max_length=150,blank=True, null=True)
    realisasi = models.IntegerField(blank=True,default='0', null=True, help_text="Pendapatan bulan ini")
    target = models.IntegerField(blank=True,default='0', null=True, help_text="Total Tagihan bulan ini")
    miss_target = models.IntegerField(blank=True,default='0', null=True, help_text="Total Tagihan tidak terbayar bulan ini")
    jml_kontributor = models.IntegerField(blank=True,default='0', null=True, help_text="jumlah orang bayar")
    jml_tagihan = models.IntegerField(blank=True,default='0', null=True, help_text="jumlah tagihan")
    jml_non_kontributor = models.IntegerField(blank=True,default='0', null=True, help_text="jumlah orang tidak bayar")
    persentase_realisasi_target = models.IntegerField(blank=True,default='0', null=True, help_text="jumlah persen terbayar / target")
    persentase_kontributor = models.IntegerField(blank=True,default='0', null=True, help_text="jumlah persen kontributor")

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.nama
    def get_absolute_url(self):
        """
        Returns the url to access a detail record for this warga.
        """
        return reverse('warga-detail', args=[str(self.id)])

    class Meta:
        db_table = 'laporan_penerimaan'
