from django.apps import AppConfig


class BookConfig(AppConfig):
    name = 'book'

class TagihanConfig(AppConfig):
    name = 'tagihan'
