from django import forms
from .models import Book, Warga ,Tagihan
from django.db.models import Q
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime #for checking renewal date range.
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _



STATUS_WARGA = (
    ('Normal', 'Normal'),
    ('SP1', 'SP1'),
    ('SP2', 'SP2')
)

STATUS_TAGIHAN = (
    ('Belum Terbayar', 'Belum Terbayar'),
    ('Lunas', 'Lunas'),
)



class TagihanForm(forms.Form):
    """docstring for ClassName"""
    #renewal_date = forms.DateField(help_text="Enter a date between now and 4 weeks (default 3).")
    #forms.DateField(help_text="Enter a date between now and 4 weeks (default 3).")
    """
    tanggal = forms.DateField(
        label="Tanggal",
        initial='',
        widget=forms.DateInput(attrs={
            'id': 'tanggal',
            'class': 'form-control',
            'data-parsley-required': 'true',
            'type': 'date'
            
        }),
        required=True)
    widget = forms.DateInput(format='%m/%Y')
    input_formats = ('%m/%Y', '%m/%y')
    #default_validators = [
    #    MinValueValidator(now().date()),
    #]

    def clean_tanggal(self):
        data = self.cleaned_data['tanggal']
        
        #Check date is not in past. 
        if data < datetime.date.today():
            raise ValidationError(_('Tanggal Salah - Masukkan data dengan benar'))

        #Check date is in range librarian allowed to change (+4 weeks).
        if data > datetime.date.today() + datetime.timedelta(weeks=4):
            raise ValidationError(_('Invalid date - renewal more than 4 weeks ahead'))

        # Remember to always return the cleaned data.
        return data

       
    no_kavling = forms.ModelChoiceField(
        queryset = Warga.objects.values_list('no_kavling',flat=True).order_by('no_kavling'),
        required=True,
        widget=forms.Select(attrs={
            'id': 'no_kavling',
            'data-parsley-required': 'true'
        }),
    )
    """
    no_kavling = forms.CharField(
        label="no_kavling",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'no_kavling',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)  
        
    pemakaian_air = forms.FloatField(
        label="pemakaian_air",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'pemakaian_air',
            'class': 'form-control',
            'data-parsley-required': 'true',
            'type': 'number'
        }),
        required=True)

    """pemakaian_lalu = forms.FloatField(
        label="pemakaian_lalu",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'pemakaian_lalu',
            'class': 'form-control',
            'data-parsley-required': 'true',
            'type': 'number'
        })
    )"""

class EditTagihanForm(forms.Form):
    status = forms.ChoiceField(
        choices=STATUS_TAGIHAN,
        label="status",
        initial='Belum Terbayar',
        widget=forms.Select(attrs={
            'id': 'status',
            'class': 'form-control',
            'data-parsley-required': 'true'
        })
    )


class ConfirmTagihanForm(forms.Form):
    """docstring for ClassName"""
    #renewal_date = forms.DateField(help_text="Enter a date between now and 4 weeks (default 3).")
    #forms.DateField(help_text="Enter a date between now and 4 weeks (default 3).")
    """
    tanggal = forms.DateField(
        label="Tanggal",
        initial='',
        widget=forms.DateInput(attrs={
            'id': 'tanggal',
            'class': 'form-control',
            'data-parsley-required': 'true',
            'type': 'date'
            
        }),
        required=True)
    widget = forms.DateInput(format='%m/%Y')
    input_formats = ('%m/%Y', '%m/%y')
    #default_validators = [
    #    MinValueValidator(now().date()),
    #]

    def clean_tanggal(self):
        data = self.cleaned_data['tanggal']
        
        #Check date is not in past. 
        if data < datetime.date.today():
            raise ValidationError(_('Tanggal Salah - Masukkan data dengan benar'))

        #Check date is in range librarian allowed to change (+4 weeks).
        if data > datetime.date.today() + datetime.timedelta(weeks=4):
            raise ValidationError(_('Invalid date - renewal more than 4 weeks ahead'))

        # Remember to always return the cleaned data.
        return data
    """
    
    no_kavling = forms.CharField(
        label="no_kavling",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'no_kavling',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
        required=True)

    pemakaian_air = forms.FloatField(
        label="pemakaian_air",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'pemakaian_air',
            'class': 'form-control',
            'data-parsley-required': 'true',
            'type': 'number'
        })
    )

    """pemakaian_lalu = forms.FloatField(
        label="pemakaian_lalu",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'pemakaian_lalu',
            'class': 'form-control',
            'data-parsley-required': 'true',
            'type': 'number'
        })
    )"""


class WargaForm(forms.Form):
    """docstring for ClassName"""  
    def clean(self):
        cleaned_data = super(WargaForm, self).clean()
        no_kavling = cleaned_data.get('no_kavling')
        cek_warga = Warga.objects.filter(no_kavling=no_kavling)
        if cek_warga.count():
            raise forms.ValidationError("No Kavling sudah ada.")

    no_va = forms.CharField(
        label="no_va",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'no_va',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
        required=True)

    nama = forms.CharField(
        label="nama",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'nama',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
        required=True)

    no_kavling = forms.CharField(
        label="no_kavling",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'no_kavling',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)
    
    email = forms.CharField(
        label="email",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'email',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    hp = forms.CharField(
        label="hp",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'hp',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    meter_air = forms.CharField(
        label="meter_air",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'meter_air',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    luas_tanah = forms.IntegerField(
        label="luas_tanah",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'luas_tanah',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    ipl = forms.IntegerField(
        label="ipl",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'ipl',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    tunggakan = forms.IntegerField(
        label="tunggakan",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'tunggakan',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    tunggakan_denda = forms.IntegerField(
        label="tunggakan_denda",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'tunggakan_denda',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    saldo = forms.IntegerField(
        label="saldo",
        initial='',
        widget=forms.TextInput(attrs={
            'id': 'saldo',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)
    denda = forms.IntegerField(
        label="denda",
        initial='0',
        widget=forms.TextInput(attrs={
            'id': 'denda',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)
    lain = forms.IntegerField(
        label="lain",
        initial='3000',
        widget=forms.TextInput(attrs={
            'id': 'lain',
            'class': 'form-control',
            'data-parsley-required': 'true'
        }),
    required=True)

    status = forms.ChoiceField(
        choices=STATUS_WARGA,
        label="status",
        initial='',
        widget=forms.Select(attrs={
            'id': 'status',
            'class': 'form-control',
            'data-parsley-required': 'true'
        })
    )


  
