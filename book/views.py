from django.shortcuts import render, HttpResponse
from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.template.loader import get_template
from django.template import Context
from django.conf import settings
from django.views import View
from django.views import generic
from .models import LaporanPenerimaan,Book, Warga ,Tagihan,income,InputTagihan,Datamutasi,ArsipInputTagihan,ArsipDatamutasi,Setting,Mutasi,Hbb,Dbb,PenyesuaianWarga
from book.forms import TagihanForm
from book.forms import ConfirmTagihanForm
from book.forms import WargaForm
from book.forms import EditTagihanForm
from django.contrib import messages
from django.db.models import Avg, Count, Min, Sum
from django.db.models import Q
import io

from django.contrib.auth.models import User

import arrow

import datetime
from datetime import datetime

from django.http import FileResponse
from reportlab.pdfgen import canvas

from io import BytesIO

from django.http import HttpResponse
from reportlab.pdfgen import canvas
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin


from django.views.generic import View
from django.utils import timezone
from .render import *
import requests
from threading import Thread, activeCount


from django.core.mail import BadHeaderError, send_mail
from django.http import HttpResponse, HttpResponseRedirect

from django.http import HttpResponse
from reportlab.pdfgen import canvas

from twilio.rest import Client
from django.conf import settings

import csv

from django.http import HttpResponse
from .models import Passenger
from django.db.models import Count, Q
from django.shortcuts import render
from django.db.models import Sum
from django.db.models.functions import ExtractYear
from django.db.models.functions import ExtractMonth
import serial

# tambahan
const_iuran_estate = 80000
const_biaya_admin = 5000
const_biaya_perawatan = 5000

#print ("Program otomatis GENERATE Tagihan")

def Terbilang(x):
    satuan=["","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan","Sepuluh","Sebelas"]
    n = int(x)
    if n >= 0 and n <= 11:
        Hasil = " " + satuan[n]
    elif n >= 12 and n <= 19:
        Hasil = Terbilang(n % 10) + " Belas"
    elif n >= 20 and n <= 99:
        Hasil = Terbilang(n / 10) + " Puluh" + Terbilang(n % 10)
    elif n >= 100 and n <= 199:
        Hasil = " Seratus" + Terbilang(n - 100)
    elif n >= 200 and n <= 999:
        Hasil = Terbilang(n / 100) + " Ratus" + Terbilang(n % 100)
    elif n >= 1000 and n <= 1999:
        Hasil = " Seribu" + Terbilang(n - 1000)
    elif n >= 2000 and n <= 999999:
        Hasil = Terbilang(n / 1000) + " Ribu" + Terbilang(n % 1000)
    elif n >= 1000000 and n <= 999999999:
        Hasil = Terbilang(n / 1000000) + " Juta" + Terbilang(n % 1000000)
    else:
        Hasil = Terbilang(n / 1000000000) + " Milyar" + Terbilang(n % 100000000)

    return Hasil

def get_Tahun(x):

    n = ExtractYear(x)
    
    return n

def Bulan_romawi(x):

    n = x
    if n == 1 :
        Hasil = "I"
    elif n == 2:
        Hasil = "II"
    elif n == 3:
        Hasil = "III"
    elif n == 4:
        Hasil = "IV"
    elif n == 5:
        Hasil = "V"
    elif n == 6:
        Hasil = "VI"
    elif n == 7:
        Hasil = "VII"
    elif n == 8:
        Hasil = "VIII"
    elif n == 9:
        Hasil = "IX"
    elif n == 10:
        Hasil = "X"
    elif n == 11:
        Hasil = "XI"
    else:
        Hasil = "XII"

    return Hasil

def hitung_tagihan_air(pemakaian_air):
    
    if pemakaian_air<=10 :
        bayar_air = 20000
    elif 10 < pemakaian_air <= 30 :
        bayar_air = pemakaian_air*2200
    elif pemakaian_air > 30 :
        bayar_air = pemakaian_air*3300

    return bayar_air

def hitung_grand_total(bayar_air):
    return bayar_air+const_iuran_estate+const_biaya_admin+const_biaya_perawatan

# end tambahan


def ticket_class_view(request):
    dataset = Tagihan.objects.annotate(
    year=ExtractYear('tanggal'),
    month=ExtractMonth('tanggal'),
    ).values('year', 'month').annotate(total_keseluruhan=Sum('total_keseluruhan'))
    print (dataset)

    dataset2 = Tagihan.objects.annotate(
    year=ExtractYear('tanggal'),
    month=ExtractMonth('tanggal'),
    ).values('year', 'month').annotate(total_keseluruhan=Sum('total_keseluruhan')).filter(status='Lunas').order_by('tanggal')
    print (dataset2)

    dataset3 = Tagihan.objects.annotate(
    year=ExtractYear('tanggal'),
    month=ExtractMonth('tanggal'),
    ).values('year', 'month').annotate(total_keseluruhan=Sum('total_keseluruhan')).filter(status='Belum Terbayar').order_by('tanggal')
    print (dataset3)

    return render(request, 'chart_income.html', {'dataset': dataset ,'dataset2': dataset2,'dataset3': dataset3})

def jumlah_tagihan_warga(request):
    dataset = Tagihan.objects.annotate(
    year=ExtractYear('tanggal'),
    month=ExtractMonth('tanggal'),
    ).values('year', 'month').annotate(total_keseluruhan=Count('total_keseluruhan'))
    print (dataset)

    dataset2 = ArsipDatamutasi.objects.annotate(
    year=ExtractYear('tanggal'),
    month=ExtractMonth('tanggal'),
    ).values('year', 'month').annotate(total_keseluruhan=Count('id'))
    print (dataset2)

    return render(request, 'chart_count_income.html', {'dataset': dataset ,'dataset2': dataset2})
"""
def ticket_class_view(request):
    dataset = Passenger.objects \
        .values('ticket_class') \
        .annotate(survived_count=Count('ticket_class', filter=Q(survived=True)),
                  not_survived_count=Count('ticket_class', filter=Q(survived=False))) \
        .order_by('ticket_class')
    return render(request, 'chart_income.html', {'dataset': dataset})
"""

def export_mutasi_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="mutasi.csv"'

    writer = csv.writer(response)    
    writer.writerow(['id','nama_pelanggan','no_pelanggan','no_kavling','nominal','tanggal','jml_bulan','dari_bulan','sampai_bulan'])
    mutasis = Mutasi.objects.raw('SELECT _datamutasi.nominal DIV warga.ipl AS jml_bulan, DATE_FORMAT(DATE_ADD(_datamutasi.tglreport, INTERVAL -((_datamutasi.nominal DIV warga.ipl )-1) MONTH),"%%M %%Y") AS dari_bulan, DATE_FORMAT(_datamutasi.tglreport, "%%M %%Y") AS sampai_bulan, DATE_FORMAT(_datamutasi.tglreport, "%%M %%Y") AS tglcatat, _datamutasi.* , warga.no_kavling, warga.ipl FROM _datamutasi,warga WHERE _datamutasi.no_pelanggan = warga.no_va')
    #mutasis = Mutasi.objects.all().values_list('id','nama_pelanggan','no_pelanggan','no_kavling','nominal','tanggal','jml_bulan','dari_bulan','sampai_bulan')
    for mutasi in mutasis:
       writer.writerow([
            mutasi.id,
            mutasi.nama_pelanggan,
            mutasi.no_pelanggan,                        
            mutasi.no_kavling,
            mutasi.nominal,
            mutasi.tanggal,
            mutasi.jml_bulan,
            mutasi.dari_bulan,
            mutasi.sampai_bulan,
            
            
       ])

    return response


def export_wargas_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="wargas.csv"'

    writer = csv.writer(response)
    writer.writerow(['id','nama','email','hp','no_kavling','meter_air','status','ipl','saldo','luas_tanah','no_va','nama_proyek','denda','lain','tunggakan','tunggakan_denda','sisa_bulan'])

    wargas = Warga.objects.all().values_list('id','nama','email','hp','no_kavling','meter_air','status','ipl','saldo','luas_tanah','no_va','nama_proyek','denda','lain','tunggakan','tunggakan_denda','sisa_bulan')
    for warga in wargas:
        writer.writerow(warga)

    return response

def export_input_bulan (request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="input_bulan_tagihan.csv"'

    writer = csv.writer(response)
    writer.writerow(['no_va', 'nama_pelanggan', 'nilai_pokok', 'tunggakan', 'denda', 'admin', 'deposit'])

    input_tagihans = InputTagihan.objects.all().values_list('no_va', 'nama_pelanggan', 'nilai_pokok', 'tunggakan', 'denda', 'admin','deposit')
    for inputtagihan in input_tagihans:
        writer.writerow(inputtagihan)

    return response


# put your own credentials here
def send_sms(nama,jatuh_tempo):
    #account_sid = "AC9cf99fa3631d6515f70ab751c30e184c"
    #auth_token = "618cd5ab2abc94cc98f39fcf7f23a04d"
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    client.messages.create(
        to="+6281703164123",
        from_="+13164444330",
        body="bpk/ibu"+nama+"Yth Tagihan estate akan jatuh tempo pd tanggal"+jatuh_tempo+"mhn abaikan jika sudah bayar")
        #media_url="https://climacons.herokuapp.com/clear.png")

def hitung_denda(no_kavling):
    num_tagihan_unpaid = Tagihan.objects.filter(status="Belum Terbayar").filter(no_kavling=no_kavling).count()
    if num_tagihan_unpaid == 0:
        result = 0
    elif num_tagihan_unpaid == 1:
        result = 10000
    elif num_tagihan_unpaid == 2:
        result = 10000
    elif num_tagihan_unpaid >= 3:
        result = 1000000  
    print (result)
    return result

def cek_status_denda(no_kavling):
    num_tagihan_unpaid = Tagihan.objects.filter(status="Belum Terbayar").filter(no_kavling=no_kavling).count()
    if num_tagihan_unpaid == 0:
        result = "normal"
    elif num_tagihan_unpaid == 1:
        result = "SP1"
    elif num_tagihan_unpaid == 2:
        result = "SP2"
    elif num_tagihan_unpaid >= 3:
        result = "SP3"  
    print (result)
    return result



def some_view(request):
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, "Hello world.")

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()
    return response

            
def send_simple_message_kwitansi(file: list,email):
    proyek = Setting.objects.get().nama_proyek
    return requests.post(
        
        #"https://api.mailgun.net/v3/sandboxf75fe74f931244c8b445d5b6ead7d729.mailgun.org/messages",
        "https://api.mailgun.net/v3/mg.provest.co.id/messages",
        auth=("api", "key-bcc26adcba3cea86bc5c7cb89f921fb5"),      
        files=[("attachment", (file[0], open(file[1], "rb").read()))],
        data={
              "from": "Estate Management"+ proyek +" <noreply@abdaelnusa.co.id>",
              "to": email,
              "subject": "["+proyek+"] Bukti Bayar IPL",
              "text": "Yang Terhormat Bapak/Ibu \n\n\
               Bersama ini kami sampaikan tanda terima pembayaran IPL "+proyek+" untuk bulan ini.\n\n\
               Terima kasih atas partisipasi Anda dalam mendukung Pengelolaan Lingkungan "+proyek+".\n\n\
    Salam hangat, \n\n\n\
    Estate Management "+proyek+"."})

def send_simple_message_invoice(file: list,email):
    proyek = Setting.objects.get().nama_proyek
    return requests.post(
        #"https://api.mailgun.net/v3/sandboxf75fe74f931244c8b445d5b6ead7d729.mailgun.org/messages",
        "https://api.mailgun.net/v3/mg.provest.co.id/messages",
        auth=("api", "key-bcc26adcba3cea86bc5c7cb89f921fb5"),      
        files=[("attachment", (file[0], open(file[1], "rb").read()))],
        data={
              "from": "Estate Management "+proyek+" <noreply@abdaelnusa.co.id>",
              "to": email,
              "subject": "["+proyek+"] Tagihan IPL",
              "text": "Yang Terhormat Bapak/Ibu \n\n\
               Bersama ini kami sampaikan Tagihan IPL "+proyek+" untuk bulan ini.\n\n\
               Terima kasih atas partisipasi Anda dalam mendukung Pengelolaan Lingkungan "+proyek+".\n\n\
    Salam hangat, \n\n\n\
    Estate Management "+proyek+"."})

def cek_penyesuaian():
    print ("Program otomatis CEK Penyesuaian")
    
    Data_penyesuaian = PenyesuaianWarga.objects.filter(status="Disetujui").values_list('no_va', 'penyesuaian_tunggakan','penyesuaian_denda','penyesuaian_sisa_bulan','status','id','deposit')        
    Tolak_Data_penyesuaian = PenyesuaianWarga.objects.filter(status="Pengajuan").values_list('id')
    i=0
    j=0

    for data in Tolak_Data_penyesuaian:
        tolak_Penyesuaian = PenyesuaianWarga.objects.get(id=data[0])
        tolak_Penyesuaian.status = "Batal"
        tolak_Penyesuaian.save()
        j=j+1

    for data in Data_penyesuaian:
        
        no_va = data[0]
        penyesuaian_tunggakan= data[1]
        penyesuaian_denda= data[2]
        penyesuaian_sisa_bulan= data[3]

        save_Penyesuaian = PenyesuaianWarga.objects.get(id=data[5])
        save_Penyesuaian.status = "Terupdate"
        save_Penyesuaian.save()

        save_Warga = Warga.objects.get(no_va=no_va)
        save_Warga.tunggakan = penyesuaian_tunggakan
        save_Warga.denda = penyesuaian_denda
        save_Warga.deposit = data[6]
        save_Warga.sisa_bulan = penyesuaian_sisa_bulan
        save_Warga.save()
        i=i+1
    print("data terupdate sebanyak =",i)
    print("data batal sebanyak =",j)
    
    print ("Cek Penyesuaian Berhasil")



def generate_bkm():
    
    print ("Program otomatis GENERATE BKM")
    singkatan_pt = Setting.objects.get().singkatan_pt
    mutasi = ArsipDatamutasi.objects.filter(branch_code = "").values_list('nominal', 'tglreport','nama_pelanggan','no_pelanggan','id')        
    temp_nominal = 0
    awal =1
    nomor=1
    nomor_dbb=0
    bulan = 1
    temp_bulan = bulan
    tahun = 2018
    temp_tgl = timezone.now()
    for i in mutasi: 
        
        print("id: ",i[4])
        datatagihan =i[3].strip()
        if datatagihan[0][0] == "9":    
            temp = "0"+datatagihan[1:11] # awalan no VA 9 diganti 0 
            datatagihan = temp
            kavling = Warga.objects.filter(no_va=datatagihan).values_list('no_kavling', flat=True)   
            print ("data mutasi =", datatagihan)                            
        else:
            kavling = Warga.objects.filter(no_va=i[3]).values_list('no_kavling', flat=True)   
        if awal == 1 :
            bulan = Bulan_romawi(i[1].month)
            tahun = i[1].year  
            temp_nominal = i[0]
            temp_tgl = i[1]
            awal = 0
            nomor_dbb = nomor_dbb +1
            save_dbb = Dbb(
                hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                dbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor)+"/"+str(nomor_dbb),
                nominal =i[0],
                tglreport = i[1],
                nama_pelanggan = i[2], 
                no_kavling = kavling[0]
            )
            save_dbb.save()        

            flag_hbb = ArsipDatamutasi.objects.get(id=i[4])
            flag_hbb.branch_code = "1"
            flag_hbb.save()        

        else:
            if  temp_tgl == i[1]: #jika tgl report sama
                bulan = Bulan_romawi(i[1].month)
                tahun = i[1].year  
                
                temp_nominal = temp_nominal + i[0]
                nomor_dbb = nomor_dbb +1
                save_dbb = Dbb(
                    hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                    dbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor)+"/"+str(nomor_dbb),
                    nominal =i[0],
                    tglreport = i[1],
                    nama_pelanggan = i[2], 
                    no_kavling = kavling[0]
                )
                save_dbb.save()         
            else :
                save_hbb = Hbb(
                    hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                    nominal =temp_nominal,
                    tglreport = temp_tgl
                )
                bulan = Bulan_romawi(i[1].month)
                tahun = i[1].year  
                save_hbb.save()                                         
                print(nomor)
                nomor=nomor+1                    
                if temp_bulan != bulan : #reset no ganti bulan
                    nomor = 1
                    temp_bulan = bulan
                nomor_dbb=1
                save_dbb = Dbb(
                    hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
                    dbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor)+"/"+str(nomor_dbb),
                    nominal =i[0],
                    tglreport = i[1],
                    nama_pelanggan = i[2], 
                    no_kavling = kavling[0]
                )
                save_dbb.save()                
                temp_nominal = i[0]
                temp_tgl = i[1]

            flag_hbb = ArsipDatamutasi.objects.get(id=i[4])
            flag_hbb.branch_code = "1"
            flag_hbb.save()        

    #input terakhir
    save_hbb = Hbb(
        hbb_code ="BM/BCA/APS/"+str(tahun)+"/"+str(bulan)+"/"+str(nomor),
        nominal =temp_nominal,
        tglreport = temp_tgl
    )
    save_hbb.save() 
    print ("Program otomatis Generate BKM Berhasil")
    print(str(nomor))


@login_required
def index(request):
    """
    View function for home page of site.
    """
    hari_ini = datetime.today()
    tahun_lalu = (hari_ini.year)
    bulan_lalu = (hari_ini.month-1) #upload before 31 bulan
    if bulan_lalu == 0 :
        bulan_lalu = 12
        tahun_lalu = hari_ini.year-1
        #bulan_ini = (hari_ini.month ) % 12 #upload after 31
    tagihan_lalu = datetime(tahun_lalu, bulan_lalu, 1)
    tagihan_sekarang = datetime(hari_ini.year, hari_ini.month, 1)
    print(tagihan_sekarang)

    login_url = '/accounts/login'
    # Generate counts of some of the main objects
    num_warga=Warga.objects.all().count()
    num_tagihan=Tagihan.objects.all().count()
    # Available tagihan unpaid (status = 'u')
    # num_tagihan_unpaid=Tagihan.objects.filter(        
    #     Q(status='Belum Terbayar') , Q(tanggal=tagihan_lalu)
    #     ).count()

    # num_tagihan_paid=Tagihan.objects.filter(
    #     Q(status='Lunas') , Q(tanggal=tagihan_lalu)
    #     ).count()

    # total_tagihan_unpaid = Tagihan.objects.filter(        
    #     Q(status='Belum Terbayar') , Q(tanggal=tagihan_lalu)
    #     ).aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00

    # total_tagihan_paid = Tagihan.objects.filter(        
    #     Q(status='Lunas') , Q(tanggal=tagihan_lalu)
    #     ).aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00

    # total_tagihan = Tagihan.objects.filter(        
    #     Q(tanggal=tagihan_lalu)
    #     ).aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00
    #context['list_tagihan'] = Tagihan.objects.raw("select tagihan.* , warga.no_kavling as no_rumah from tagihan,warga where warga.no_kavling like %s and tagihan.no_kavling = warga.no_va order by tagihan.id desc", [request.GET.get('kavling')] )
    
    total_deposit = Tagihan.objects.filter(
        Q(tanggal=tagihan_lalu)
        ).aggregate(Sum('deposit'))['deposit__sum'] or 0.00

    total_tunggakan = Tagihan.objects.filter(
        Q(tanggal=tagihan_lalu)
        ).aggregate(Sum('tunggakan'))['tunggakan__sum'] or 0.00

    total_iuran = Tagihan.objects.filter(
        Q(tanggal=tagihan_lalu)
        ).aggregate(Sum('iuran_estate'))['iuran_estate__sum'] or 0.00
    
    jml_deposit = Tagihan.objects.filter(
        ~Q(deposit=0) ,Q(tanggal=tagihan_lalu)
        ).aggregate(Count('deposit'))['deposit__count'] or 0.00

    jml_tunggakan = Tagihan.objects.filter(
        ~Q(tunggakan=0) ,Q(tanggal=tagihan_lalu)
        ).aggregate(Count('tunggakan'))['tunggakan__count'] or 0.00
    
    jml_normal = Tagihan.objects.filter(
        Q(deposit=0) ,Q(tunggakan=0),Q(tanggal=tagihan_lalu)
        ).aggregate(Count('iuran_estate'))['iuran_estate__count'] or 0.00
    
    jml_deposit_terbayar = Tagihan.objects.filter(
        Q(status='Lunas') ,~Q(deposit=0),Q(tanggal=tagihan_lalu)
        ).aggregate(Count('deposit'))['deposit__count'] or 0.00
    jml_tunggakan_terbayar = Tagihan.objects.filter(
        Q(status='Lunas') ,~Q(tunggakan=0),Q(tanggal=tagihan_lalu)
        ).aggregate(Count('tunggakan'))['tunggakan__count'] or 0.00
    jml_normal_terbayar = Tagihan.objects.filter(
        Q(status='Lunas') ,Q(deposit=0),Q(tunggakan=0),Q(tanggal=tagihan_lalu)
        ).aggregate(Count('iuran_estate'))['iuran_estate__count'] or 0.00

    persen_deposit_terbayar = jml_deposit_terbayar / jml_deposit *100
    persen_tunggakan_terbayar = jml_tunggakan_terbayar /jml_tunggakan *100
    persen_normal_terbayar = jml_normal_terbayar /jml_normal *100

    
    
    # Render the HTML template index.html with the data in the context variable
    return render(
        request,
        'index.html',
        context={
            'num_warga':num_warga,
            'num_tagihan':num_tagihan,
            'total_deposit':total_deposit,
            'total_tunggakan':total_tunggakan,
            'total_iuran':total_iuran,

            'jml_deposit':jml_deposit,
            'jml_tunggakan':jml_tunggakan,
            'jml_normal':jml_normal,

            'jml_deposit_terbayar':jml_deposit_terbayar,
            'jml_tunggakan_terbayar':jml_tunggakan_terbayar,
            'jml_normal_terbayar':jml_normal_terbayar,
            
            'persen_deposit_terbayar':persen_deposit_terbayar,
            'persen_tunggakan_terbayar':persen_tunggakan_terbayar,
            'persen_normal_terbayar':persen_normal_terbayar,
            'periode':tagihan_lalu
            
            }
    )


#class PDF
class PdfTagihan(View):

    def get_context_data(self):
        """list_tagihan = Tagihan.objects.filter(id=fid)
        today = timezone.now()
        params = {
            'today': today,
            'list_tagihan': list_tagihan,
            'request': request
        }"""
        
        #return Render.render('pdftagihan.html', params)
        return (Context)
        #file = Render.render_to_file('pdf.html', params)
        #thread = Thread(target=send_email, args=(file,))
        #thread.start()
        #return HttpResponse("Processed")

class GenerateLaporanWarga(View):

    def get(self, request):
        list_warga = Warga.objects.all()
        today = timezone.now()
        params = {
            'today': today,
            'list_warga': list_warga,
            'request': request
        }
        return Render.render('laporanwarga.html', params)

class GenerateLaporanIncome(View):

    def get(self, request):
        list_tagihan = Tagihan.objects.all()
        total_tagihan = Tagihan.objects.aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00
        total_tagihan_unpaid = Tagihan.objects.filter(status='Belum Terbayar').aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00
        total_tagihan_paid = Tagihan.objects.filter(status='Lunas').aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00

        today = timezone.now()
        params = {
            'today': today,
            'total_tagihan':total_tagihan,
            'total_tagihan_unpaid': total_tagihan_unpaid,
            'total_tagihan_paid':total_tagihan_paid,
            'list_tagihan': list_tagihan,
            'request': request
        }
        return Render.render('laporanincome.html', params)

class AnalyticsIndexView(TemplateView):
    template_name = 'chart_income.html'

    def get_context_data(self, **kwargs):
        context = super(AnalyticsIndexView, self).get_context_data(**kwargs)
        context['30_day_registrations'] = self.thirty_day_registrations()
        return context

    def thirty_day_registrations(self):
        final_data = []

        date = arrow.now()
        for day in xrange(1, 30):
            date = date.replace(days=-1)
            count = User.objects.filter(
                date_joined__gte=date.floor('day').datetime,
                date_joined__lte=date.ceil('day').datetime).count()
            final_data.append(count)

        return final_data

class GenerateChartIncome(View):

    def get(self, request):
        list_tagihan = income.objects.all()
        #total_tagihan = Tagihan.objects.aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00
        #total_tagihan_unpaid = Tagihan.objects.filter(status='Belum Terbayar').aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00
        #total_tagihan_paid = Tagihan.objects.filter(status='Lunas').aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00

        today = timezone.now()
        params = {
            'today': today,
            #'total_tagihan':total_tagihan,
            #'total_tagihan_unpaid': total_tagihan_unpaid,
            #'total_tagihan_paid':total_tagihan_paid,
            'list_tagihan': list_tagihan,
            'request': request
        }
        return Render.render('chart_income.html', params)
    




class TagihanCetakView(View):

    def get(self, request,fid,no_kavling):
        context={
        }
 
        list_tagihan = Tagihan.objects.filter(id=fid)
        status = Tagihan.objects.get(id=fid).status 
        list_warga = Warga.objects.filter(no_va=no_kavling)
        email = Warga.objects.get(no_va=no_kavling).email
        setting = Setting.objects.all()
        today = timezone.now()
        params = {
            
            'today': today,
            'list_tagihan': list_tagihan,
            'list_warga': list_warga,
            'setting': setting,
            'request': request
        }
        print (fid)
        print (email)
        print (status)
        if status == "Lunas" :
        #if status == "LUNAS" :
            print ("masuk kwitansi")
            print (email)
            file = Render.render_to_file('cetakkwitansi.html', params)
            #thread = Thread(target=send_simple_message_kwitansi, args=(file,email,))
            #thread.start()
            return Render.render('cetakkwitansi.html', params)
        elif status == "Belum Terbayar"  :
        #elif status == "Belum Terbayar" :
            print ("masuk tagihan")
            print (email)
            file = Render.render_to_file('cetaktagihan.html', params)
            #thread = Thread(target=send_simple_message_invoice, args=(file,email,))
            #thread.start()
            return Render.render('cetaktagihan.html', params)

class CetakInvoiceView(View):

    def get(self, request,fid,no_va):
        context={
            #'tagihan' : Tagihan.objects.get(id=fid),          
            #'warga' : Warga.objects.filter(no_kavling='B91')
            #'warga' : Warga.objects.get(no_kavling='B91').nama #select meter_air from warga where no kavling = x
        }
        #print (no_kavling)
        list_tagihan = Tagihan.objects.filter(id=fid)
        status = Tagihan.objects.get(id=fid).status 
        list_warga = Warga.objects.filter(no_va=no_va)
        email = Warga.objects.get(no_va=no_va).email
        today = timezone.now()
        setting = Setting.objects.all()
        today = timezone.now()        
            
        params = {
            'setting': setting,
            'today': today,
            'list_tagihan': list_tagihan,
            'list_warga': list_warga,
            'request': request
        }

        if status == "Belum Terbayar" and email != "-" :
        #elif status == "Belum Terbayar" :
            print ("masuk tagihan")
            print (email)
            file = Render.render_to_file('cetaktagihan.html', params)
            thread = Thread(target=send_simple_message_invoice, args=(file,email,))
            thread.start()
            return Render.render('cetaktagihan.html', params)

class CetakKwitansiView(View):

    def get(self, request,fid,no_kavling):
        context={
          
        }
        list_tagihan = Tagihan.objects.filter(id=fid)
        status = Tagihan.objects.get(id=fid).status 
        list_warga = Warga.objects.filter(no_va=no_kavling)
        email = Warga.objects.get(no_va=no_kavling).email
        today = timezone.now()
        params = {
            
            'today': today,
            'list_tagihan': list_tagihan,
            'list_warga': list_warga,
            'request': request
        }
        #print (status)
        #send_simple_message2()
        #send_sms("nico","27 May 1989")
        print (fid)
        print (email)
        print (status)
        #if status == "Lunas" and email != "-":
        if  email != "-":
            print ("masuk sent email")
            print (email)
            file = Render.render_to_file('cetakkwitansi.html', params)
            thread = Thread(target=send_simple_message_kwitansi, args=(file,email,))
            thread.start()
            return Render.render('cetakkwitansi.html', params)
        

class IndexTemplateWargaView(LoginRequiredMixin,TemplateView):
    login_url = '/accounts/login'
    #redirect_field_name = 'redirect_to'
    template_name = "home_warga.html"
    
   
    def get_context_data(self, *args, **kwargs):
        context = super(IndexTemplateWargaView, self).get_context_data(*args, **kwargs)
        context["title"] = "Add Warga"
        context['list_warga'] = Warga.objects.all()

        return context

class IndexTemplateMutasiView(LoginRequiredMixin,TemplateView):
    login_url = '/accounts/login'
    #redirect_field_name = 'redirect_to'
    template_name = "home_mutasi.html"

    http_method_names = ['get', 'post']

    def get(self,request, *args, **kwargs):
        
        if 'tanggal' in request.GET  :
            print (request.GET.get('tanggal'))
            context = super(IndexTemplateMutasiView, self).get_context_data(*args, **kwargs)
            context["title"] = "Home Mutasi"
            context['list_mutasi'] = Mutasi.objects.filter(tanggal = request.GET.get('tanggal')).order_by('tanggal')
            return render(request, template_name=self.template_name, context=context)
        else:
            context = super(IndexTemplateMutasiView, self).get_context_data(*args, **kwargs)
            context["title"] = "Home Mutasi"
            #context['list_mutasi'] = Mutasi.objects.all().order_by('tanggal')
            context['list_mutasi'] = Mutasi.objects.raw('SELECT _datamutasi.nominal DIV warga.ipl AS jml_bulan, DATE_FORMAT(DATE_ADD(_datamutasi.tglreport, INTERVAL -((_datamutasi.nominal DIV warga.ipl )-1) MONTH),"%%M %%Y") AS dari_bulan, DATE_FORMAT(_datamutasi.tglreport, "%%M %%Y") AS sampai_bulan, DATE_FORMAT(_datamutasi.tglreport, "%%M %%Y") AS tglcatat, _datamutasi.* , warga.no_kavling, warga.ipl FROM _datamutasi,warga WHERE _datamutasi.no_pelanggan = warga.no_va')
            #context['list_mutasi'] = Mutasi.objects.raw('SELECT _datamutasi.nominal DIV warga.ipl AS jml_bulan, DATE_FORMAT(_datamutasi.tglreport, "%%M %%Y"), _datamutasi.* , warga.no_kavling, warga.ipl FROM _datamutasi,warga WHERE _datamutasi.no_pelanggan = warga.no_va')
            return render(request, template_name=self.template_name, context=context)
            
   
    #def get_context_data(self, *args, **kwargs):
        #context = super(IndexTemplateMutasiView, self).get_context_data(*args, **kwargs)
        #context["title"] = "Home Mutasi"
        #context['list_mutasi'] = Mutasi.objects.all().order_by('tanggal')

        #return context

class IndexTemplateArsipMutasiView(LoginRequiredMixin,TemplateView):
    login_url = '/accounts/login'
    #redirect_field_name = 'redirect_to'
    template_name = "home_arsip_mutasi.html"

    http_method_names = ['get', 'post']

    def get(self,request, *args, **kwargs):
        #if 'q' in request.GET != "":
        #if request.GET.get('kavling') != "" or request.GET.get('status') == "" :
        if 'tanggal' in request.GET  :

            print (request.GET.get('tanggal'))
            context = super(IndexTemplateMutasiView, self).get_context_data(*args, **kwargs)
            context["title"] = "Home Arsip Mutasi"
            context['list_mutasi'] = ArsipDatamutasi.objects.filter(tglreport = request.GET.get('tanggal')).order_by('tanggal')
            return render(request, template_name=self.template_name, context=context)
        else:
            context = super(IndexTemplateArsipMutasiView, self).get_context_data(*args, **kwargs)
            context["title"] = "Home Mutasi"
            #context['list_mutasi'] = ArsipDatamutasi.objects.all().order_by('tglreport')
            context['list_mutasi'] = ArsipDatamutasi.objects.raw('select arsipdatamutasi.* , warga.no_kavling from arsipdatamutasi,warga where arsipdatamutasi.no_pelanggan = warga.no_va')

            return render(request, template_name=self.template_name, context=context)


class IndexTemplateSettingView(LoginRequiredMixin,TemplateView):
    login_url = '/accounts/login'
    #redirect_field_name = 'redirect_to'
    template_name = "home_setting.html"
    
   
    def get_context_data(self, *args, **kwargs):
        context = super(IndexTemplateSettingView, self).get_context_data(*args, **kwargs)
        context["title"] = "Home Setting"
        context['list_setting'] = Setting.objects.all()

        return context

class IndexTemplateTagihanView(LoginRequiredMixin,TemplateView):
    login_url = '/accounts/login'
    template_name = "home_tagihan.html"
    http_method_names = ['get', 'post']

    def get(self,request, *args, **kwargs):
        #if 'q' in request.GET != "":
        #if request.GET.get('kavling') != "" or request.GET.get('status') == "" :
        if 'kavling' in request.GET or 'status' in request.GET :
            context = super(IndexTemplateTagihanView, self).get_context_data(*args, **kwargs)
            context["title"] = "Daftar Tagihan"
            #context['list_tagihan'] = Tagihan.objects.filter(no_kavling__contains=request.GET.get('kavling'),status__contains=request.GET.get('status')).order_by('id').reverse
            
            context['list_tagihan'] = Tagihan.objects.raw("select tagihan.* , warga.no_kavling as no_rumah from tagihan,warga where warga.no_kavling like %s and tagihan.no_kavling = warga.no_va order by tagihan.id desc", [request.GET.get('kavling')] )
            return render(request, template_name=self.template_name, context=context)
        else:
            form = TagihanForm()
            now = datetime.date(datetime.now())
            context = super(IndexTemplateTagihanView, self).get_context_data(*args, **kwargs)
            context["title"] = "Add Tagihan"
            context["today"] = datetime.now()
            context["hari_ini"] = datetime.date(datetime.now())
            #context['list_tagihan'] = Tagihan.objects.all().order_by('-id')[:1000][::-1]
            context['list_tagihan'] = Tagihan.objects.raw('select tagihan.* , warga.no_kavling as no_rumah from tagihan,warga where tagihan.no_kavling = warga.no_va order by tagihan.id desc' )
            
            

            return render(request, template_name=self.template_name, context=context)

        
        
class DetailTagihanWargaView(TemplateView):
    template_name = "detail_tagihan_warga.html"

    def get_context_data(self,fid, *args, **kwargs):
        context = super(DetailTagihanWargaView, self).get_context_data(*args, **kwargs)
        context['list_tagihan'] = Tagihan.objects.filter(no_kavling=fid)

        return context

class IndexTemplateView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, *args, **kwargs):
        context = super(IndexTemplateView, self).get_context_data(*args, **kwargs)
        context["title"] = "Add Book"
        context['list_book'] = Book.objects.all()

        return context


class BookAddView(View):
    
    template_name = "formbook.html"
    http_method_names = ['get', 'post']
    def get(self,request, *args, **kwargs):

        form = BookForm()
        context={
            'form':form
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self,request,*args, **kwargs):

        form = BookForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            author = form.cleaned_data['author']
            data_published = form.cleaned_data['data_published']
            number_of_page = form.cleaned_data['number_of_page']
            type_of_book = form.cleaned_data['type_of_book']

            saveBook = Book(
                title = title,
                author = author,
                data_published = data_published,
                number_of_page = number_of_page,
                type_of_book = type_of_book
            )
            saveBook.save()
            return HttpResponseRedirect("/index")


        context = {
            'form': form
        }
        return render(request, template_name=self.template_name, context=context)


class TagihanAddView(View):
    template_name = "addtagihan.html"
    http_method_names = [ 'get','post']
    
    def get(self,request, *args, **kwargs):

        
        form = TagihanForm()
        context={
            'form':form,             
        }
        return render(request, template_name=self.template_name, context=context)
    



    def post(self,request,*args, **kwargs):
       

        form = TagihanForm(request.POST)
        print (form.data['no_kavling'])
        #print (form.cleaned_data['no_kavling'])
        
        #if 'kavling' in request.POST :
        #    print (request.GET.get('kavling'))
        
        if form.is_valid():      
            no_kavling = form.cleaned_data['no_kavling']
            cek_avail_kavling = Warga.objects.filter(no_kavling=no_kavling)
            if cek_avail_kavling.count()<=0:
                messages.warning(request,'Kavling no'+ no_kavling +' Tidak ditemukan')
            else : 
                pemakaian_air_lalu =Warga.objects.get(no_kavling=no_kavling).meter_air #select meter_air from warga where no kavling = x
                pemakaian_air = form.cleaned_data['pemakaian_air']-pemakaian_air_lalu
                if 0 <= pemakaian_air <=100 :
                    cek_jatuh_tempo = datetime(datetime.now().year, datetime.now().month+1, 15)
                    cek_data_kembar = Tagihan.objects.filter(no_kavling=no_kavling,jatuh_tempo=cek_jatuh_tempo)

                    if cek_data_kembar.count()>0:
                        messages.warning(request, 'Tagihan Untuk Kavling '+ no_kavling +' bulan ini sudah terdaftar')
                
                    else : 
                
                        iuran_estate = const_iuran_estate
                        total_pembayaran_air = hitung_tagihan_air(pemakaian_air)
                        total_denda = hitung_denda(no_kavling)
                        total_keseluruhan = hitung_grand_total(total_pembayaran_air)+total_denda
                        # update master warga
                        Warga.objects.filter(no_kavling=no_kavling).update(meter_air=form.cleaned_data['pemakaian_air'])
                        Warga.objects.filter(no_kavling=no_kavling).update(status=cek_status_denda(no_kavling))        
                        saveTagihan = Tagihan(
                        #tanggal = tanggal,
                            tanggal = timezone.now(),
                            meteran_air_lalu =pemakaian_air_lalu,
                            meteran_air_sekarang =  form.cleaned_data['pemakaian_air'],
                            no_kavling = no_kavling,
                            pemakaian_air = pemakaian_air,
                
                            biaya_admin = const_biaya_admin,
                            biaya_perawatan = const_biaya_perawatan,

                            iuran_estate = iuran_estate,
                            total_pembayaran_air = total_pembayaran_air,
                            total_keseluruhan = total_keseluruhan,
                            jatuh_tempo = datetime(datetime.now().year, datetime.now().month+1, 15),
                            denda = total_denda
                        )
                
                        saveTagihan.save()  
                        #TagihanCetakView.get(self, request,saveTagihan.id,saveTagihan.no_kavling) #sent email
                        messages.success(request, 'Tagihan Berhasil Ditambahkan!', extra_tags='alert')
                        #ConfirmTagihanAddView.get(self,request,tanggal,pemakaian_air_lalu,pakai_air,no_kavling,pemakaian_air,iuran_estate,total_pembayaran_air,total_keseluruhan)
                        return HttpResponseRedirect(reverse('list_tagihan') )
                else:
                    messages.warning(request, 'Inputan meter air Salah. Meter Air Lalu : '+ str(pemakaian_air_lalu ) + ', pemakaian air harus antara '+str(pemakaian_air_lalu)+'-'+str(pemakaian_air_lalu+100))  # <-
        else:
            messages.error(request, "Error")
        #else:
        #    messages.warning(request, 'Pastikan data yang anda isi benar.')  # <-


        context = {
            'form': form
        }
        return render(request, template_name=self.template_name, context=context)


class TagihanEditView(View):
    template_name = "edittagihan.html"
    http_method_names = ['get', 'post']
    
    def get(self,request,fid, *args, **kwargs):
        form = EditTagihanForm()
        context={
            'form':form, 
            'tagihan' : Tagihan.objects.get(id=fid)          
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self,request,*args, **kwargs):
        id = request.POST['id']
        if request.method == "POST":
            print(request.POST['tanggal'])
            tagihan = Tagihan.objects.get(id=id)
            tagihan.tanggal = request.POST['tanggal']
            tagihan.no_kavling = request.POST['no_kavling']
            tagihan.pemakaian_air = request.POST['pemakaian_air']
            tagihan.iuran_estate = request.POST['iuran_estate']
            tagihan.total_pembayaran_air = request.POST['total_pembayaran_air']
            tagihan.total_keseluruhan = request.POST['total_keseluruhan']
            tagihan.status = request.POST['status']
            tagihan.save()
            if tagihan.status == "Lunas" :
                messages.success(request, 'Pembayaran Berhasil Diproses!', extra_tags='alert')
                #TagihanCetakView.get(self, request,saveTagihan.id,saveTagihan.no_kavling) #sent email                

            return HttpResponseRedirect(reverse('list_tagihan') )


        context = {
            'tagihan' : Tagihan.objects.get(id=id)
        }
        return render(request, template_name=self.template_name, context=context)


class WargaAddView(View):
    template_name = "addwarga.html"
    http_method_names = ['get', 'post']
    def get(self,request, *args, **kwargs):

        form = WargaForm()
        context={
            'form':form
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self,request,*args, **kwargs):
        nama_proyek = Setting.objects.get().nama_proyek        
        form = WargaForm(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            no_kavling = form.cleaned_data['no_kavling']
            #nama_proyek = form.cleaned_data['nama_proyek']
            email = form.cleaned_data['email']
            hp = form.cleaned_data['hp']
            meter_air = form.cleaned_data['meter_air']
            luas_tanah = form.cleaned_data['luas_tanah']
            ipl = form.cleaned_data['ipl']
            tunggakan = form.cleaned_data['tunggakan']
            denda = form.cleaned_data['denda']
            tunggakan_denda = form.cleaned_data['tunggakan_denda']
            saldo = form.cleaned_data['saldo']
            status = form.cleaned_data['status']
            no_va = form.cleaned_data['no_va']
          
            saveWarga = Warga(
                nama = nama,
                no_kavling = no_kavling,
                nama_proyek = nama_proyek,
                email = email,
                hp = hp,
                meter_air = meter_air,
                luas_tanah = luas_tanah,
                ipl = ipl,
                tunggakan = tunggakan,
                denda =denda,
                tunggakan_denda = tunggakan_denda,
                saldo = saldo,
                status = status,
                no_va = no_va
            )
            saveWarga.save()
            return HttpResponseRedirect(reverse('list_warga') )


        context = {
            'form': form
        }
        return render(request, template_name=self.template_name, context=context)
        #return render(request, 'catalog/book_renew_librarian.html', {'form': form, 'bookinst':book_inst})

class GenerateTagihanWargaView(View):
    template_name = "generatetagihanwarga.html"
    http_method_names = ['get', 'post']
    def post(self,request, *args, **kwargs):

        form = WargaForm()
        context={
            'form':form
        }
        
        hari_ini = datetime.today()
        bulan_ini = (hari_ini.month+1 ) % 12 #upload before 31 bulan
        if bulan_ini == 0 :
            bulan_ini = 12
        #bulan_ini = (hari_ini.month ) % 12 #upload after 31
        bulan_tagihan = datetime(hari_ini.year, bulan_ini, 1)
        num_tagihan=InputTagihan.objects.all().count()
        if num_tagihan > 0 :
            messages.warning(request,'Tagihan sudah pernah diproses, lakukan cek payment terlebih dahulu ', extra_tags='alert')
            
        else:
            wargas = Warga.objects.all().values_list('no_va', 'nama', 'ipl','tunggakan','denda','lain','sisa_bulan','deposit')
            for warga in wargas:
                if  warga[6] <= 0 :   # jika tidak punya sisa bulan         
                    saveinput_tagihan = InputTagihan(
                        no_va =warga[0],
                        nama_pelanggan =warga[1],
                        nilai_pokok = warga[2],
                        tunggakan =warga[3],
                        denda = warga[4],
                        admin = warga[5],
                        deposit =warga[7],
                    )
                    saveinput_tagihan.save()
                    saveTagihan = Tagihan(
                        tanggal = bulan_tagihan,
                        meteran_air_lalu =0,
                        meteran_air_sekarang =  0,              
                        pemakaian_air = 0,                
                        biaya_admin = warga[5],
                        biaya_perawatan = 0,
                        no_kavling = warga[0],
                        iuran_estate = warga[2],
                        tunggakan = warga[3],
                        deposit = warga[7],
                        total_pembayaran_air = 0,
                        total_keseluruhan = warga[2]+warga[3]+warga[4]+warga[5]+warga[7],
                        jatuh_tempo = datetime(datetime.now().year, bulan_ini, 15),
                        denda = warga[4]
                    )     
                    saveTagihan.save()  
                    #CetakInvoiceView.get(self, request,saveTagihan.id,saveTagihan.no_kavling) #sent email
                else : # jika punya saldo
                    no_va= warga[0]
                    sisa_bulan = warga[6]-1
                    #hitung_tunggak =  0 # warga[3] + warga[2]
                    #hitung_deposit = warga[7] - warga[2]
                    Warga.objects.filter(no_va=no_va).update(sisa_bulan=sisa_bulan)
                    Warga.objects.filter(no_va=no_va).update(tunggakan=0)
                    Warga.objects.filter(no_va=no_va).update(saldo=0)
                    #CetakInvoiceView.get(self, request,saveTagihan.id,saveTagihan.no_kavling) #sent email
            messages.success(request, 'Tagihan Berhasil Ditambahkan!', extra_tags='alert')       
        return render(request, template_name=self.template_name, context=context)

    def get(self,request,*args, **kwargs):
        form = WargaForm()
        context={
            'form':form
        }
        return render(request, template_name=self.template_name, context=context)

class GenerateInvoiceTagihanView(View):
    template_name = "emailtagihanwarga.html"
    http_method_names = ['get', 'post']
    def post(self,request, *args, **kwargs):

        form = WargaForm()
        context={
            'form':form
        }                
        num_tagihan=InputTagihan.objects.all().count()
        hari_ini = datetime.today()
        bulan_ini = (hari_ini.month+1 ) % 12 #upload before 31 bulan
        if bulan_ini == 0 :
            bulan_ini = 12
        bulan_tagihan = datetime(hari_ini.year, bulan_ini, 1)
                   
        wargas = Tagihan.objects.filter(tanggal=bulan_tagihan).values_list('id','no_kavling')
        for warga in wargas:                
            CetakInvoiceView.get(self, request,warga[0],warga[1]) #sent email                
        messages.success(request, 'Tagihan Berhasil Ditambahkan!', extra_tags='alert')       
        return render(request, template_name=self.template_name, context=context)

    def get(self,request,*args, **kwargs):
        form = WargaForm()
        context={
            'form':form
        }
        return render(request, template_name=self.template_name, context=context)

        

class GenerateBkmView(TemplateView):
    template_name = "generate_bkm.html"
    http_method_names = ['get', 'post']
    def post(self,request, *args, **kwargs):

        generate_bkm()
        messages.success(request, 'BKM Berhasil Digenerate!', extra_tags='alert')       
        context = super(GenerateBkmView, self).get_context_data(*args, **kwargs)
        context["title"] = "Home Bkm"
        context['list_bkm'] = Hbb.objects.all().order_by('tglreport') 
        return render(request, template_name=self.template_name, context=context)

        #generate BKM dari awal
        """ mutasi = ArsipDatamutasi.objects.all().values_list('nominal', 'tglreport','nama_pelanggan','no_pelanggan','id')        
        temp_nominal = 0
        awal =1
        nomor=1
        nomor_dbb=0
        bulan = 1
        temp_bulan = bulan
        tahun = 2018
        for i in mutasi: 
            print(i[4])
            datatagihan =i[3].strip()
            if datatagihan[0][0] == "9":    
                temp = "0"+datatagihan[1:11] # awalan no VA 9 diganti 0 
                datatagihan = temp
                kavling = Warga.objects.filter(no_va=datatagihan).values_list('no_kavling', flat=True)   
                print ("data mutasi =", datatagihan)                            
            else:
                kavling = Warga.objects.filter(no_va=i[3]).values_list('no_kavling', flat=True)   
            if awal == 1 :
                bulan = Bulan_romawi(i[1].month)
                tahun = i[1].year  
                temp_nominal = i[0]
                temp_tgl = i[1]
                awal = 0
                nomor_dbb = nomor_dbb +1
                save_dbb = Dbb(
                    hbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor),
                    dbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor)+"-"+str(nomor_dbb),
                    nominal =i[0],
                    tglreport = i[1],
                    nama_pelanggan = i[2], 
                    no_kavling = kavling[0]
                )
                save_dbb.save()                 
            else:
                if  temp_tgl == i[1]: #jika tgl report sama
                    bulan = Bulan_romawi(i[1].month)
                    tahun = i[1].year  
                    
                    temp_nominal = temp_nominal + i[0]
                    nomor_dbb = nomor_dbb +1
                    save_dbb = Dbb(
                        hbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor),
                        dbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor)+"-"+str(nomor_dbb),
                        nominal =i[0],
                        tglreport = i[1],
                        nama_pelanggan = i[2], 
                        no_kavling = kavling[0]
                    )
                    save_dbb.save()         
                else :
                    save_hbb = Hbb(
                        hbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor),
                        nominal =temp_nominal,
                        tglreport = temp_tgl
                    )

                    bulan = Bulan_romawi(i[1].month)
                    
                    tahun = i[1].year  
                    save_hbb.save()                                         
                    print(nomor)
                    nomor=nomor+1                    
                    if temp_bulan != bulan : #reset no ganti bulan
                        nomor = 1
                        temp_bulan = bulan
                    nomor_dbb=1
                    save_dbb = Dbb(
                        hbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor),
                        dbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor)+"-"+str(nomor_dbb),
                        nominal =i[0],
                        tglreport = i[1],
                        nama_pelanggan = i[2], 
                        no_kavling = kavling[0]
                    )
                    save_dbb.save()
                    
                    temp_nominal = i[0]
                    temp_tgl = i[1]

        #input terakhir
        save_hbb = Hbb(
            hbb_code ="BM-BCA-APS-"+str(tahun)+"-"+str(bulan)+"-"+str(nomor),
            nominal =temp_nominal,
            tglreport = temp_tgl
        )
        save_hbb.save() 

        print(str(nomor))
       
        return render(request, template_name=self.template_name)
 """
    def get(self,request,*args, **kwargs):

        context = super(GenerateBkmView, self).get_context_data(*args, **kwargs)
        context["title"] = "Home Bkm"
        context['list_bkm'] = Hbb.objects.all().order_by('tglreport') 
        return render(request, template_name=self.template_name, context=context)

class DetailBkm(TemplateView):
    template_name = "detail_bkm.html"

    def get_context_data(self,fid, *args, **kwargs):
        context = super(DetailBkm, self).get_context_data(*args, **kwargs)
        kode = Hbb.objects.filter(id=fid).values_list('hbb_code', flat=True).first()
        context['list_detail_bkm'] = Dbb.objects.filter(hbb_code=kode)
        return context



class BkmCetakView(View):

    def get(self, request,fid):
        hbb = Hbb.objects.filter(id=fid)
        kode = str(Hbb.objects.filter(id=fid).values_list('hbb_code', flat=True))
        kode_hbb = Hbb.objects.filter(id=fid).values_list('hbb_code', flat=True).first()
        list_dbb = Dbb.objects.filter(hbb_code=kode_hbb) 
        nominal = Hbb.objects.filter(id=fid).values_list('nominal', flat=True)
        nominal = int(nominal[0])
        temp = str(kode[1])
        temp.replace("-", "/")
        print(temp)
        hasil = Terbilang(str(nominal)) 
        
        params = {           
            'hbb': hbb,
            'kode':kode,
            'list_dbb': list_dbb,
            'hasil': hasil,
            'request': request
        }
        file = Render.render_to_file('cetakbkm.html', params)
        return Render.render('cetakbkm.html', params)
        


class CekTagihanWargaView(View):
    
    template_name = "cektagihanwarga.html"
    http_method_names = ['get', 'post']

    def post(self,request,*args, **kwargs):
        if request.method == "POST":
            form = WargaForm()
            context={
                'form':form
            }
            num_tagihan=InputTagihan.objects.all().count()
            if num_tagihan > 0 :
                hari_ini = datetime.today()
                bulan_ini = (hari_ini.month ) % 12 #upload before 31 bulan
                if bulan_ini == 0 :
                    bulan_ini = 12
                #bulan_ini = (hari_ini.month ) % 12 #upload after 31
                bulan_tagihan = datetime(hari_ini.year, bulan_ini, 1)
                
                total_tagihan = Tagihan.objects.filter(tanggal=bulan_tagihan).aggregate(Sum('total_keseluruhan'))['total_keseluruhan__sum'] or 0.00
                total_tagihan_paid = Datamutasi.objects.aggregate(Sum('nominal'))['nominal__sum'] or 0.00                
                total_tagihan_unpaid = total_tagihan-total_tagihan_paid
                persentase = total_tagihan_paid/total_tagihan*100
        
                i = 0
                j = 0
                k = 0
                cek_ada=0
                listtagihan = InputTagihan.objects.all().values_list('no_va','denda','nilai_pokok','tunggakan','id') #datatagihan
                #listmutasi = Datamutasi.objects.all().values_list('no_pelanggan','tanggal') #datamutasi masuk
                listmutasi = Datamutasi.objects.filter(company_code='07458').values_list('no_pelanggan','tanggal','nominal') #datamutasi masuk
            
                listtagihanall = Tagihan.objects.all().values_list('id','no_kavling') #datamutasi masuk

                jatuh_tempo = datetime(datetime.now().year, datetime.now().month, 15)
                persen_denda = Setting.objects.get().denda

                for tagihan in listtagihan: # sebanyak isi table inputtagihan
                    print (i)
                    datatagihan =tagihan[0].strip()
                    id_tagihan = tagihan[4]
                    if datatagihan[0][0] != "9":  # jika tidak bayar tahunan
                        iuran_ipl = Warga.objects.get(no_va=datatagihan).ipl
                        tunggakan_denda =  Warga.objects.get(no_va=datatagihan).tunggakan_denda
                    for mutasi in listmutasi: # sebanyak data di table _datamutasi
                        datamutasi =mutasi[0].strip()
                        if datatagihan == datamutasi: #(jika dibayar)
                            if datatagihan[0][0] == "9":    
                                temp = "0"+datatagihan[1:11] # awalan no VA 9 diganti 0 
                                datatagihan = temp
                                print ("data mutasi =", datatagihan)

                            k=k+1
                            print ('bayar:'+datatagihan)
                            print (id_tagihan)
                            Tagihan.objects.filter(no_kavling=datatagihan).update(status="Lunas") #update status tagihan = Lunas                 
                                                        
                            Warga.objects.filter(no_va=datatagihan).update(tunggakan=0) #update tunggakan tagihan = 0
                            Warga.objects.filter(no_va=datatagihan).update(bayar_lalu=mutasi[2]) #update pembayaran bulan lalu
                            if mutasi[1] > datetime.date(jatuh_tempo): 
                                bayar_denda = persen_denda * iuran_ipl /100
                                #Warga.objects.filter(no_va=datatagihan).update(denda=bayar_denda) #update denda tagihan = 0 disable denda

                                #hitung_tunggakan_denda = bayar_denda + tunggakan_denda
                                
                                #Warga.objects.filter(no_va=datatagihan).update(tunggakan_denda=hitung_tunggakan_denda) #update denda tagihan = 0
                            else :
                                Warga.objects.filter(no_va=datatagihan).update(denda=0) #update denda tagihan = 0
                                Warga.objects.filter(no_va=datatagihan).update(tunggakan_denda=0) #update denda tagihan = 0
                            
                            cek_sisa_bulan = Warga.objects.filter(no_va=datatagihan).values_list('sisa_bulan', flat=True)   # cek pembayaran tahunan                    
                            cek_sisa_bulan = int(cek_sisa_bulan[0])
                            if cek_sisa_bulan<0 : # jika bayar tahunan
                                
                                cek_sisa_bulan = abs(cek_sisa_bulan) - 1
                                print(cek_sisa_bulan)
                                print(datatagihan)
                                Warga.objects.filter(no_va=datatagihan).update(sisa_bulan=cek_sisa_bulan)
                                Warga.objects.filter(no_va=datatagihan).update(deposit=0)
                                
                                #CetakKwitansiView.get(self, request,id_tagihan,datatagihan) #sent email kwitansi
                            i = i + 1
                            cek_ada = 1

                #(jika tidak bayar)
                    if cek_ada == 0 :
                        print ('tidak bayar:'+datatagihan)
                        j= j + 1
                        bayar_denda = persen_denda * iuran_ipl /100
                        hitung_tunggakan =tagihan[2]+tagihan[3] #IPL+tunggakan tidak hitung admin_VA
                        Warga.objects.filter(no_va=datatagihan).update(tunggakan=hitung_tunggakan) #update tunggakan master warga
                        Warga.objects.filter(no_va=datatagihan).update(bayar_lalu=0) #update pembayaran bulan lalu
                        #Warga.objects.filter(no_va=datatagihan).update(denda=bayar_denda) # update denda master warga (disable)
                        #hitung_tunggakan_denda = bayar_denda + tunggakan_denda
                        
                        #Warga.objects.filter(no_va=datatagihan).update(tunggakan_denda=hitung_tunggakan_denda) #update denda tagihan = 0
                    cek_ada = 0
                    i=i+1
                

                backup_inputtagihan = InputTagihan.objects.all().values()
                for x in backup_inputtagihan:
                    ArsipInputTagihan.objects.create(**x)
                    InputTagihan.objects.all().delete()

                backup_datamutasi = Datamutasi.objects.all().values()
                for y in backup_datamutasi:
                    ArsipDatamutasi.objects.create(**y)
                    Datamutasi.objects.all().delete()

                print ("Total Tagihan tercek" ,k+j, "Pembayaran")
                print ("Pembayaran diterima Sebanyak" ,k, "Pembayaran")
                print ("Pembayaran belum diterima Sebanyak" ,j, "Pembayaran")
                                

                #cek_penyesuaian()
                #generate_bkm()
                saveLaporan = LaporanPenerimaan(
                    tanggal = datetime.date(datetime.now()),
                    realisasi = total_tagihan_paid,
                    target = total_tagihan,
                    miss_target = total_tagihan_unpaid,
                    jml_kontributor = k,
                    jml_tagihan = k+j,
                    jml_non_kontributor = j,
                    persentase_realisasi_target = persentase,          
                    persentase_kontributor = (k/(k+j))*100           
                )
                saveLaporan.save()
                
                messages.success(request, 'Tagihan Berhasil Dicheck!', extra_tags='alert')
    
            else:
                messages.warning(request,'Tidak ada Payment yang diproses ', extra_tags='alert')

            return render(request, template_name=self.template_name, context=context)

    def get(self,request, *args, **kwargs):

        form = WargaForm()
        context={
            'form':form
        }
        return render(request, template_name=self.template_name, context=context)

class SettingEditView(View):
    template_name = "editsetting.html"
    http_method_names = ['get', 'post']
    def get(self,request,fid, *args, **kwargs):
        context={
            'setting' : Setting.objects.get(id=fid)          
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self,request,*args, **kwargs):
        id = request.POST['id']
        if request.method == "POST":
            print(request.POST['nama'])
            setting = Setting.objects.get(id=id)
            setting.nama = request.POST['ipl']
            setting.no_kavling = request.POST['admin']
            setting.email = request.POST['denda']
            setting.save()
            return HttpResponseRedirect(reverse('list_setting') )


        context = {
            'setting' : Setting.objects.get(id=id)
        }
        return render(request, template_name=self.template_name, context=context)

class WargaEditView(View):
    template_name = "editwarga.html"
    http_method_names = ['get', 'post']
    def get(self,request,fid, *args, **kwargs):
        context={
            'warga' : Warga.objects.get(id=fid)          
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self,request,*args, **kwargs):
        id = request.POST['id']
        if request.method == "POST":
            print(request.POST['nama'])
            warga = Warga.objects.get(id=id)
            warga.nama = request.POST['nama']
            warga.no_va = request.POST['no_va']
            warga.no_kavling = request.POST['no_kavling']
            warga.email = request.POST['email']
            warga.hp = request.POST['hp']
            warga.meter_air = request.POST['meter_air']
            warga.status = request.POST['status']
            warga.tunggakan = request.POST['tunggakan'] #tambahan
            warga.denda = request.POST['denda'] #tambahan
            warga.lain = request.POST['lain'] #tambahan
            warga.sisa_bulan = request.POST['sisa_bulan'] #tambahan
            warga.saldo = request.POST['saldo'] #tambahan
            warga.save()
            return HttpResponseRedirect(reverse('list_warga') )


        context = {
            'warga' : Warga.objects.get(id=id)
        }
        return render(request, template_name=self.template_name, context=context)

class TahunanWargaView(View):
    template_name = "tahunanwarga.html"
    http_method_names = ['get', 'post']
    def get(self,request,fid, *args, **kwargs):
        context={
            'warga' : Warga.objects.get(id=fid)          
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self,request,*args, **kwargs):
        id = request.POST['id']               
        if request.method == "POST":
            #bulan = request.POST.get('bayar_bulan');
            
            penyesuaian_tunggakan = request.POST.get('penyesuaian_tunggakan');
            penyesuaian_denda = request.POST.get('penyesuaian_denda');
            warga = Warga.objects.get(id=id)
            temp = warga.sisa_bulan
            no_va = warga.no_va
            deposito = 0
            penyesuaian_sisa_bulan = temp
            
            if request.POST.get('bayar_bulan') != "0"  :
                ipl = warga.ipl  
                bulan = request.POST.get('bayar_bulan');
                total_bulan = int(request.POST.get('gratis_bulan'))+int(request.POST.get('bayar_bulan'));
                deposito = (int(ipl) * (int(bulan)-1)) # dikurangi biaya IPL berjalan
                #penyesuaian_tunggakan = pembayaran
                penyesuaian_sisa_bulan = total_bulan - (2*total_bulan) #dinegasikan total bulan

            save_penyesuaian = PenyesuaianWarga(
                no_va =no_va,
                deposit = deposito,
                penyesuaian_tunggakan =penyesuaian_tunggakan,
                penyesuaian_denda =penyesuaian_denda,
                penyesuaian_sisa_bulan = penyesuaian_sisa_bulan, #dinegasikan total bulan
                status = "Pengajuan", 
                tanggal = datetime(datetime.now().year, datetime.now().month, datetime.now().day),
                user = request.user.username
            )
            print(no_va,penyesuaian_denda,penyesuaian_sisa_bulan,penyesuaian_tunggakan)
            save_penyesuaian.save()
            return HttpResponseRedirect(reverse('list_warga') )
        
        context = {
            'warga' : Warga.objects.get(id=id)
        }
        return render(request, template_name=self.template_name, context=context)

class BookDeleteView(View):
    http_method_names = ['get']
    def get(self,request,fid,*args,**kwargs):
        isntance =Book.objects.get(id=fid)
        isntance.delete()
        return HttpResponseRedirect("/")

# generic view to show the details of a particular object 
class DetailsView(generic.DetailView):
    model = Tagihan
    template_name = 'template/detail.html'

class BookEditView(View):
    template_name = "editformbook.html"
    http_method_names = ['get', 'post']
    def get(self,request,fid, *args, **kwargs):
        context={
            'book' : Book.objects.get(id=fid)          
        }
        return render(request, template_name=self.template_name, context=context)

    def post(self,request,*args, **kwargs):
        id = request.POST['id']
        if request.method == "POST":
            print(request.POST['title'])
            book = Book.objects.get(id=id)
            book.title = request.POST['title']
            book.author = request.POST['author']
            book.data_published = request.POST['data_published']
            book.number_of_page = request.POST['number_of_page']
            book.type_of_book = request.POST['type_of_book']
            book.save()
            return HttpResponseRedirect("/")


        context = {
            'book' : Book.objects.get(id=id)
        }
        return render(request, template_name=self.template_name, context=context)

class WargaPenyesuaianView(TemplateView):
    template_name = "home_penyesuaian.html"
    http_method_names = ['get', 'post']
    def post(self,request, *args, **kwargs):
        tes=request.POST.get('no_va');
        save_PenyesuaianWarga = PenyesuaianWarga.objects.get(no_va=tes, status='Pengajuan')
        #save_PenyesuaianWarga = PenyesuaianWarga.objects.raw('select id from penyesuaianwarga where no_va = '+tes+' and status =Pengajuan')        
        save_PenyesuaianWarga.status = "Disetujui"
        save_PenyesuaianWarga.user = request.user.username
        save_PenyesuaianWarga.save()
        context = super(WargaPenyesuaianView, self).get_context_data(*args, **kwargs) 
        context["title"] = "Penyesuaian Warga"
        context['list_penyesuaian'] = PenyesuaianWarga.objects.all().order_by('id') 
        return render(request, template_name=self.template_name, context=context)

        
        
    def get(self,request,*args, **kwargs):
        context = super(WargaPenyesuaianView, self).get_context_data(*args, **kwargs) 
        context["title"] = "Penyesuaian Warga"
        #context['list_penyesuaian'] = PenyesuaianWarga.objects.all().order_by('id') #nico
        context['list_penyesuaian'] = Tagihan.objects.raw("select PenyesuaianWarga.* , warga.no_kavling as no_rumah from PenyesuaianWarga,warga where PenyesuaianWarga.no_va = warga.no_va order by PenyesuaianWarga.id desc" )
        
        return render(request, template_name=self.template_name, context=context)

class CekPenyesuaianView(TemplateView):
    template_name = "cek_penyesuaian.html"
    http_method_names = ['get', 'post']
    def post(self,request, *args, **kwargs):
        form = WargaForm()
        context={
            'form':form
        }
        cek_penyesuaian() 
        messages.success(request, 'Tagihan Berhasil Dicheck!', extra_tags='alert')
        return render(request, template_name=self.template_name, context=context)

        
        
    def get(self,request,*args, **kwargs):
        form = WargaForm()
        context={
            'form':form
        }
        return render(request, template_name=self.template_name, context=context)
        

def upload_csv(request):
	data = {}
    
	if "GET" == request.method:
		return render(request, "../templates/upload_csv.html", data)
    # if not GET, then proceed
	try:
        #rp = Setting.objects.get().IPL
		csv_file = request.FILES["csv_file"]
        
    
		if not csv_file.name.endswith('.csv'):
			messages.error(request,'File is not CSV type')
			return HttpResponseRedirect(reverse("upload_csv"))
            
        #if file is too large, return
		if csv_file.multiple_chunks():
			messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
			return HttpResponseRedirect(reverse('upload_csv'))

		file_data = csv_file.read().decode("utf-8")		

		lines = file_data.split("\n")
		print(lines)
    	#loop over the lines and save them in db. If error , store as string and then display
                  
		for line in lines:                            

			fields = line.split(";")									
			if fields[0] != 'Kavling':                                
			    saveWarga = Warga(                    
                    nama = fields[3],
                    no_kavling = fields[0],
                    nama_proyek = "Grand Harvest",
                    email = fields[4],
                    hp = fields[5],                	
                    luas_tanah = float(fields[1]),
                    meter_air = 0,
                    ipl = float(fields[1])*2400,
                    tunggakan = 0,
                    denda =0,
                    lain = 3000,
                    tunggakan_denda = 0,
                    saldo = 0,
                    status = "Normal",
                    no_va = fields[2]
                    )
			    saveWarga.save()    
		messages.success(request, 'Warga Berhasil Ditambahkan!', extra_tags='alert')  
		return HttpResponseRedirect(reverse('list_warga'))              
        
    
	except Exception as e:
		logging.getLogger("error_logger").error("Unable to upload file. "+repr(e))
		messages.error(request,"Unable to upload file. "+repr(e))

	return HttpResponseRedirect(reverse('upload_csv'))

class IndexLaporanPenerimaanView(LoginRequiredMixin,TemplateView):
    login_url = '/accounts/login'
    #redirect_field_name = 'redirect_to'
    template_name = "laporan_penerimaan.html"
    
   
    def get_context_data(self, *args, **kwargs):
        context = super(IndexLaporanPenerimaanView, self).get_context_data(*args, **kwargs)
        context["title"] = "Laporan Penerimaan"
        context['list_laporan'] = LaporanPenerimaan.objects.all()

        return context
